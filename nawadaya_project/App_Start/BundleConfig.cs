﻿using System.Web;
using System.Web.Optimization;

namespace nawadaya_project
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/indexlayoutcss").Include(
                "~/Content/lib/advanced-datatable/css/demo_page.css",
                "~/Content/lib/advanced-datatable/css/demo_table.css",
                "~/Content/dataTables.bootstrap.css",
                "~/Content/lib/bootstrap/css/bootstrap.min.css",
                "~/Content/css/zabuto_calendar.css",
                "~/Content/lib/gritter/css/jquery.gritter.css",
                "~/Content/css/style.css",
                "~/Content/css/style-responsive.css",
                "~/Content/lib/chart-master/Chart.js"

          ));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            "~/Scripts/jquery.unobtrusive*",
            "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/Content/indexlayoutjs").Include(
               "~/Content/lib/advanced-datatable/js/jquery.js",
               "~/Layout/jquery.dataTables.js",
               "~/Layout/DT_bootstrap.js",
               "~/Layout/jquery.min.js",
               "~/Layout/dataTables.bootstrap.js"
 ));

            bundles.Add(new StyleBundle("~/Content/rekruitmen").Include(
            "~/Content/all.min.css",
            "~/Content/magnific-popup.css",
            "~/Content/creative.min.css",
            "~/Content/creative.css",
            "~/Content/vendor",
            "~/Content/fontawesome-free/css/all.min.css"
            ));
            bundles.Add(new ScriptBundle("~/Login/index").Include(
             "~/Login/jquery.min.js",
             "~/Login/bootstrap.min.js",
             "~/Login/jquery.backstretch.min.js",
                            "~/Content/jquery.dcjqaccordion.2.7.js",
               "~/Content/jquery.scrollTo.min.js",
               "~/Content/jquery.nicescroll.js",
               "~/Content/common-scripts.js",
                          "~/Content/jquery.backstretch.min.js"
             ));

            bundles.Add(new StyleBundle("~/Layout/index").Include(
             "~/Content/font-awesome.css",
             "~/Content/font-awesome.min.css"

             ));


            bundles.Add(new ScriptBundle("~/bundles/rekruitmen").Include(
             "~/Scripts/vendor/jquery/jquery.min.js",
             "~/Scripts/vendor/bootstrap/js/bootstrap.bundle.min.js",
           "~/Scripts/vendor/jquery-easing/jquery.easing.min.js",
           "~/Scripts/vendor/magnific-popup/jquery.magnific-popup.min.js",
           "~/Scripts/js/creative.min.js"
                    ));
            BundleTable.EnableOptimizations =true;

        }
    }
}
