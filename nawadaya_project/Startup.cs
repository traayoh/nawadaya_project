﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(nawadaya_project.Startup))]
namespace nawadaya_project
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
