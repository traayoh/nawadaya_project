﻿using nawadaya_project.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace project_nawadaya.Controllers
{
    public class ExaminerController : Controller
    {
        // GET: Examiner
        private Nawadaya_Entities db = new Nawadaya_Entities();
        public ActionResult Index()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var listassign = (from s in db.ms_logictest_result where s.logictest_score == null select s.logictest_score).ToList();
            int countassign = 0;
            foreach (var item in listassign)
            {
                countassign++;
            }
            ViewBag.Assign = countassign;
            var listunassign = (from s in db.ms_logictest_result where s.logictest_score != null select s.logictest_score).ToList();
            int countunassign = 0;
            foreach (var item in listunassign)
            {
                countunassign++;
            }
            ViewBag.Unassign = countunassign;

            ViewBag.HistoricalExaminer = (from s in db.logictest_examiner select s).ToList();

            return View();

        }

        public ActionResult Check()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data = (from s in db.logictest_examiner where   s.logictest_score==null && s.status_employee == 1 select s).ToList();
                ViewBag.MsExaminer = data;
                ViewBag.AddScore = TempData["AddScore"];
                ViewBag.Name = TempData["Name"];
                ViewBag.message = TempData["message"];
            }

            return View();
        }

        //public ActionResult Check2()
        //{
        //    if (Session["UserName"] == null)
        //    {
        //        return RedirectToAction("Index", "Login");
        //    }
        //    else
        //    {
        //        var data = (from s in db.logictest_examiner where s.logictest_score == null && s.status_employee == 3 select s).ToList();
        //        ViewBag.MsExaminer = data;
        //        ViewBag.AddScore = TempData["AddScore"];
        //        ViewBag.Name = TempData["Name"];
        //        ViewBag.message = TempData["message"];
        //    }

        //    return View();
        //}

        //public ActionResult Result2()
        //{
        //    if (Session["UserName"] == null)
        //    {
        //        return RedirectToAction("Index", "Login");
        //    }
        //    else
        //    {
        //        var data2 = (from s in db.logictest_examiner where s.logictest_score != null && s.status_employee == 3 select s).ToList();
        //        ViewBag.MsHistorical = data2;
        //    }

        //    return View();
        //}

        public ActionResult Result()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data2 = (from s in db.logictest_examiner where s.logictest_score != null && s.status_employee==1 select s).ToList();
                ViewBag.MsHistorical = data2;
            }

            return View();
        }

        [HttpGet]
        public ActionResult AddScore(int id)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
                var query = (from s in db.ms_logictest_result where s.pk_logictest_result == id select s).FirstOrDefault();
                var data = (from s in db.logictest_examiner where s.pk_logictest_result == id select s.full_name).FirstOrDefault();
                 TempData["AddScore"] = 1;
                 TempData["Name"] = data;
                    ViewBag.AddScore = TempData["AddScore"];
                    ViewBag.Name = TempData["Name"];


            return View("Check",query);
        }

        [HttpPost]
        public ActionResult InputScore(ms_logictest_result data1)
        {

            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var data = db.ms_logictest_result.Find(data1.pk_logictest_result);
                    data.created_by = Session["UserName"].ToString();
                    data.logictest_score = data1.logictest_score;
                    data.note = data1.note;
                    data.date_created = DateTime.Now;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("Check");
        }

    }
}