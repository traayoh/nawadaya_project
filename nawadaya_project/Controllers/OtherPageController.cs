﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace nawadaya_project.Controllers
{
    public class OtherPageController : Controller
    {
        // GET: OtherPage
        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Unableaccess()
        {
            return View();
        }
    }
}