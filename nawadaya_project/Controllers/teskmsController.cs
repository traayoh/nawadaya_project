﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using nawadaya_project.Models;

namespace nawadaya_project.Controllers
{
    public class teskmsController : Controller
    {
        private Nawadaya_Entities db = new Nawadaya_Entities();
        // GET: teskms
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult UZIP(int id)
        {
            //ViewBag.Message = "Your application description page.";
            ms_logictest_result obj_update = new ms_logictest_result();
            obj_update = db.ms_logictest_result.Where(x => x.pk_logictest_result == id).First();
            return View(obj_update);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UZIP(ms_logictest_result obj_zip, HttpPostedFileBase file)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (file.ContentLength > 0 && Path.GetExtension(file.FileName).ToUpper() == ".ZIP")
                    {
                        string _FileName = Path.GetFileName(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/Uploaded_LogicTestSolutions"), _FileName);
                        //string _FileExt = ;
                        file.SaveAs(_path);
                        ViewBag.Message = "File Uploaded Successfully!!";

                        obj_zip.logictest_solutions = _FileName;
                        obj_zip.logictest_solutions_path = "~/Uploaded_LogicTestSolutions";
                        db.Entry(obj_zip).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        //return RedirectToAction("Index");
                        return View();
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                    return View();
                }

            }

        }
    }
}