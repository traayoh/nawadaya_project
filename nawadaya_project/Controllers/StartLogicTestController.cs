﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using nawadaya_project.Models;
using System.Security.Cryptography;
using System.Text;

namespace nawadaya_project.Controllers
{
    public class StartLogicTestController : Controller
    {
        private Nawadaya_Entities db = new Nawadaya_Entities();
        // GET: StartLogicTest
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StartAndRedirect(long id, string link)
        {
            var key = "b14ca5898a4e4133bbce2ea2315a1916";
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (id != 0)
                    {
                        int active = 1;
                        var obj_pdf_problems = db.historical_invitation_progress.Where(x => x.fk_applicant_information_form == id && x.access_token == active).FirstOrDefault();//ini buat ambil objek/baris di kolom histrocal_invitation yang page logic_test masih bisa diakses(access_token=1) oleh pengguna (fk_applicant_information_form)
                        if (obj_pdf_problems.logic_test_first_access == 1) 
                        {
                            List<ms_logictest_result> list_latest_examinee = db.ms_logictest_result.Where(x => x.fk_applicant_information_form == (int)id).ToList();
                            var obj_latest_examinee_pk = list_latest_examinee.OrderBy(item => item.pk_logictest_result).Last();
                            TempData["link"] = link;
                            var latest_examinee_pk = obj_latest_examinee_pk.pk_logictest_result;
                            return RedirectToAction("UploadZIP", new { pk_id2 = EncryptString(key,latest_examinee_pk.ToString())  });
                        }
                        //ms_logictest_result obj = db.ms_logictest_result.Find(id);
                        ms_logictest_result obj = new ms_logictest_result();
                        obj.fk_applicant_information_form = Convert.ToInt32(id);
                        var logictest_duration = Int32.Parse((db.ms_all_for_one.Where(x => x.pk_all_for_one == 1).FirstOrDefault()).var_value);
                        obj.end_time = DateTime.UtcNow.AddSeconds(logictest_duration);
                        
                        //var obj_pdf_problems = db.historical_invitation_progress.Where(x => x.fk_applicant_information_form == (long)obj.fk_applicant_information_form && x.access_token == active).FirstOrDefault();//ini buat ambil objek/baris di kolom histrocal_invitation yang page logic_test masih bisa diakses(access_token=1) oleh pengguna (fk_applicant_information_form)
                        obj.fk_logictest_problems = obj_pdf_problems.fk_logictest_problems;
                        db.ms_logictest_result.Add(obj);

                        //start of /make logictest user can only access StartAndRedirect action one time
                        obj_pdf_problems.logic_test_first_access=1;
                        db.Entry(obj_pdf_problems).State = EntityState.Modified;

                        //end of /make logictest user can only access StartAndRedirect action one time

                        //db.ms_employee.Remove(obj_ms_employee);

                        db.SaveChanges();
                        transaction.Commit();
                        var pk_id = obj.pk_logictest_result;
                        TempData["link"] = link;
                        return RedirectToAction("UploadZIP", new { pk_id2 = EncryptString(key, pk_id.ToString())  });
                    }
                    return View();
                }
                catch (Exception ex)
                {

                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                    return View();

                }
            }

        }

        [HttpGet]
        public ActionResult UploadZIP(string pk_id2, string FileDetails)
        {
            var key = "b14ca5898a4e4133bbce2ea2315a1916";
            var obj1 = pk_id2.Replace(" ", "+");
            var pk_id = Convert.ToInt64(DecryptString(key, obj1)); 
            ViewBag.link = TempData["link"];
            var obj = db.ms_logictest_result.Where(x => x.pk_logictest_result == (int)pk_id).FirstOrDefault();
            ViewBag.EndTime_LT = obj.end_time;

            var obj2 = db.ms_personal_data.Where(s => s.pk_employee == obj.fk_applicant_information_form).FirstOrDefault();
            ViewBag.Email = obj2.email;

            ViewBag.ID =EncryptString(key, obj.pk_logictest_result.ToString());

            ViewBag.FileDetails = FileDetails;

            return View();
        }

        [HttpPost]
        public ActionResult UploadZIP(HttpPostedFileBase file,string link,string email,string pk_id2)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var key = "b14ca5898a4e4133bbce2ea2315a1916";
                    var obj1 = pk_id2.Replace(" ", "+");
                    var pk_id = Convert.ToInt64(DecryptString(key, obj1));
                    if (file.ContentLength > 0 && Path.GetExtension(file.FileName).ToUpper() == ".ZIP" && Path.GetFileName(file.FileName).Length < 105)
                    {
                        //ViewBag.EndTime = obj.end_time;
                        int fk_applicant = Convert.ToInt16((from s in db.ms_personal_data where s.email == email select s.pk_employee).FirstOrDefault());
                        string _FileName = Path.GetFileName(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/Uploaded_LogicTestSolutions"), _FileName);
                        //string _FileExt = ;
                        file.SaveAs(_path);
                        ViewBag.Message = "File Uploaded Successfully!!";
                        var obj_zip = db.ms_logictest_result.Where(x => x.pk_logictest_result == (int)pk_id).FirstOrDefault();
                        obj_zip.logictest_solutions = _FileName;
                        //obj_zip.fk_applicant_information_form = fk_applicant;
                        obj_zip.logictest_solutions_path = "~/Uploaded_LogicTestSolutions";
                        obj_zip.date_created = DateTime.Now;
                        obj_zip.date_test = DateTime.Now;
                        obj_zip.created_by = email;
                        //var obtest = db.historical_invitation_progress.Where(x => x.pk_historical_invitation_progress == (int)obj_zip.fk_applicant_information_form).FirstOrDefault
                        db.Entry(obj_zip).State = EntityState.Modified;
                        db.SaveChanges();
                        long pk_rekrut = (from s in db.ms_recruit where s.fk_applicant_information_form == fk_applicant select s.pk_recruitID).FirstOrDefault();
                        var query4 = db.ms_recruit.Find(pk_rekrut);
                        if (pk_rekrut != 0)
                        {
                            query4.data_crated = DateTime.Now;
                            query4.fk_logictest_result = obj_zip.pk_logictest_result;
                            db.Entry(query4).State = EntityState.Modified;
                        }
                        else
                        {
                            var x = new ms_recruit();
                            x.fk_logictest_result = obj_zip.pk_logictest_result;
                            x.data_crated = DateTime.Now;
                            db.ms_recruit.Add(x);
                        }
                        var logicdone = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == fk_applicant && s.access_token == 1 select s).FirstOrDefault();
                        logicdone.logictest_status = 3;
                        db.Entry(logicdone).State = EntityState.Modified;
                        TempData["message"] = 1;
                        db.SaveChanges();
                        transaction.Commit();
                        TempData["message"] = 3;
                        var obj_link = db.historical_invitation_progress.Where(x => x.fk_applicant_information_form == fk_applicant && x.access_token == 1).FirstOrDefault();
                        var enc_link = obj_link.link;
                        return RedirectToAction("Index", "Recruitment", new { obj = enc_link });
                        
                        //return View();
                    }
                    else if (Path.GetFileName(file.FileName).Length >= 105)
                    {
                        ViewBag.FileDetails = "Filename is too long.";
                        var obj = db.ms_logictest_result.Where(x => x.pk_logictest_result == (int)pk_id).FirstOrDefault();
                        ViewBag.EndTime_LT = obj.end_time;

                        var obj2 = db.ms_personal_data.Where(s => s.pk_employee == obj.fk_applicant_information_form).FirstOrDefault();
                        ViewBag.Email = obj2.email;
                        TempData["link"] = link;
                        return RedirectToAction("UploadZIP", new { pk_id2 = EncryptString(key, pk_id.ToString()), FileDetails = ViewBag.FileDetails });
                    }
                    else 
                    {
                        ViewBag.FileDetails = "Invalid file format.";
                        var obj = db.ms_logictest_result.Where(x => x.pk_logictest_result == (int)pk_id).FirstOrDefault();
                        ViewBag.EndTime_LT = obj.end_time;

                        var obj2 = db.ms_personal_data.Where(s => s.pk_employee == obj.fk_applicant_information_form).FirstOrDefault();
                        ViewBag.Email = obj2.email;
                        TempData["link"] = link;
                        return RedirectToAction("UploadZIP", new { pk_id2 = EncryptString(key, pk_id.ToString()), FileDetails = ViewBag.FileDetails });

                    }
                    //return View();

                }
                catch
                {
                    ViewBag.Message = "File upload failed!!";
                    return View();
                }
            }
        }
        public ActionResult viewpdfnewtab(string id2)
        {
            var key = "b14ca5898a4e4133bbce2ea2315a1916";
            var obj1 = id2.Replace(" ", "+");
            var id = Convert.ToInt64(DecryptString(key, obj1));
            //ViewBag.Message = "Your application description page.";
            var obj = db.ms_logictest_result.Where(x => x.pk_logictest_result == (int)id).FirstOrDefault();
            ViewBag.EndTime_LT = obj.end_time;

            var obj2 = db.ms_personal_data.Where(s => s.pk_employee == obj.fk_applicant_information_form).FirstOrDefault();
            ViewBag.Email = obj2.email;

            var obj_path = db.ms_logictest_problems.Where(x => x.pk_logictest_problems_id == obj.fk_logictest_problems).FirstOrDefault();
            string folderpath = obj_path.problems_path;
            string filename = obj_path.problems_filename;
            //string chosenPDF_path = Path.Combine(Server.MapPath(folderpath), filename);
            string chosenPDF_path = folderpath + "/" + filename;
            ViewBag.PDF = chosenPDF_path;

            return View();
        }

        public static string EncryptString(string key, string plainInput)
        {
            byte[] iv = new byte[16];
            byte[] array;
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainInput);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }
        public static string DecryptString(string key, string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

    }
}