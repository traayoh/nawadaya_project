﻿using nawadaya_project.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace project_nawadaya.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            if (Session["hak_akses"] != null)
            {
                var hak = Session["hak_akses"].ToString();
                if (hak == "Admin")
                {
                    return RedirectToAction("Index", "HR");
                }
                else if (hak == "Super Admin")
                {
                    return RedirectToAction("Index", "RM");
                }
                else
                {
                    return RedirectToAction("Index", "Examiner");
                }
            }
            else
            {
                ViewBag.messagesuccess = TempData["messagesuccess"];
                ViewBag.ResetSuccess = TempData["ResetSuccess"];
                return View();
            }

        }

        private Nawadaya_Entities db = new Nawadaya_Entities();
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(login objUser)
        {
            if (ModelState.IsValid)
            {
                {
                    var password = md5(objUser.password);
                    var obj = (from s in db.logins where s.email.Equals(objUser.email) && s.password.Equals(password) select s).FirstOrDefault();
                    if (obj != null)
                    {
                        Session["UserID"] = obj.PK_userID.ToString();
                        Session["FullName"] = obj.full_name.ToString();
                        Session["Email"] = obj.email.ToString();
                        Session["UserName"] = obj.username.ToString();
                        Session["hak_akses"] = obj.role_name.ToString();
                        string hak = obj.role_name.ToString();
                        if (hak == "Admin")
                        {
                            return RedirectToAction("Index", "HR");
                        }
                        else if (hak == "Super Admin")
                        {
                            return RedirectToAction("Index", "RM");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Examiner");
                        }
                    }
                    else
                    {
                        ViewBag.MessageUnknown = "User Unknown";
                        return View();
                    }
                }
            }
            return View(objUser);
        }

        [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Logoff(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Session["UserName"] = null;
            Session.RemoveAll();
            Session.Abandon();

            return RedirectToAction("Index", "RM");
        }

        [HttpPost]
        public ActionResult Forgot(String email)
        {
            var email1 = email;
            var data = (from s in db.ms_personal_data where s.email.Equals(email1) select s.pk_employee).FirstOrDefault();
            var data2 = (from s in db.tbl_user where s.fk_employee == data select s).FirstOrDefault();
            if (data2 != null)
            {
                var key = "b14ca5898a4e4133bbce2ea2315a1916";
                var link = EncryptString(key, email);
                var fullname = db.ms_personal_data.Where(x => x.email == email).FirstOrDefault();
                var validation = SendEmail(email, link, fullname.full_name);
                if (validation == true)
                {

                    TempData["messagesuccess"] = 1;
                }
                else
                {
                    TempData["messagesuccess"] = 0;
                }
            }
            else {
                TempData["messagesuccess"] = 2;
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public bool SendEmail(string email, string link, string fullname)
        {
            MailMessage mm = new MailMessage("prewtry7@gmail.com", email);
            mm.From = new MailAddress("prewtry7@gmail.com", "Nawadaya");
            mm.Subject = "Forgot password";
            string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi "+ fullname + ",</b><br>Someone requested a new password for your Nawadaya account. Click the button below to reset it.<br><br> <a class='button' href='http://localhost/Nawadaya2/Login/Reset?id=" + link + "'>Reset Password</a><br>If you didn't make this request then you can safely ignore this email.";
            mm.Body = tulisan;
            mm.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;

            NetworkCredential nc = new NetworkCredential("prewtry7@gmail.com", "husehusen");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = nc;
            try
            {
                smtp.Send(mm);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                RedirectToAction("NotFound", "OtherPage");
                return false;
            }

        }

        [HttpGet]
        public ActionResult Reset(string id)
        {
            if (Session["UserName"] != null)
            {
                RedirectToAction("Index");
            }
            
            
                id = id.Replace(" ", "+");
                var key = "b14ca5898a4e4133bbce2ea2315a1916";
                var email = DecryptString(key, id);
                var separate = email.Split('=');
                email = separate[0];
                long query = (from s in db.ms_personal_data where s.email.Equals(email) select s.pk_employee).FirstOrDefault();
                long query2 = (from s in db.tbl_user where s.fk_employee == query select s.pk_userID).FirstOrDefault();
                ViewBag.pk_userid = query2;
            ViewBag.id = id;
            ViewBag.messagesuccess = TempData["ResetSuccess"];
            return View();
            

            //return View();
        }

        //[HttpGet]
        //public ActionResult New_User(string id)
        //{
        //    if (Session["UserName"] != null)
        //    {
        //        RedirectToAction("Index");
        //    }
            
        //        id = id.Replace(" ", "+");
        //        var key = "b14ca5898a4e4133bbce2ea2315a1916";
        //        var email = DecryptString(key, id);
        //        var separate = email.Split('=');
        //        email = separate[0];
        //        long query = (from s in db.ms_personal_data where s.email.Equals(email) select s.pk_employee).FirstOrDefault();
        //        long query2 = (from s in db.tbl_user where s.fk_employee == query select s.pk_userID).FirstOrDefault();
        //        ViewBag.pk_userid = query2;
        //        return View();
            
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult new_user(tbl_user item)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var data = db.tbl_user.Find(item.pk_userID);
        //        var key = "b14ca5898a4e4133bbce2ea2315a1916";
        //        var password = EncryptString(key, string.Format("{0}", item.password));
        //        data.password = password;
        //        data.username = item.username;
        //        db.Entry(data).State = EntityState.Modified;
        //        db.SaveChanges();
        //        TempData["ResetSuccess"] = 2;
        //    }

        //    return RedirectToAction("Index");
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Reset(tbl_user item, string password2,string id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var data = db.tbl_user.Find(item.pk_userID);
                        var password = md5(item.password);
                        if (password == md5(password2))
                        {
                            data.password = password;
                            db.Entry(data).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["ResetSuccess"] = 1;
                        }
                        else {
                            TempData["ResetSuccess"] = 2;
                            return Redirect("http://localhost/Nawadaya2/Login/Reset?id="+id);
                        }

                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return RedirectToAction("Index");
        }

        static string md5(string input)
        { // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create(); // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes // and create a string.
            StringBuilder sBuilder = new StringBuilder(); // Loop through each byte of the hashed data // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string EncryptString(string key, string plainInput)
        {
            byte[] iv = new byte[16];
            byte[] array;
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainInput);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }
        public static string DecryptString(string key, string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }



    }
}