﻿using Microsoft.Reporting.WebForms;
using nawadaya_project.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace nawadaya_project.Controllers
{
    public class HRController : Controller
    {
        // GET: HR
        private Nawadaya_Entities db = new Nawadaya_Entities();
        public ActionResult Index()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var listemployee = db.ms_personal_data.Where(x=>x.status_employee==3).ToList();
            int countemployee = 0;
            foreach (var item in listemployee)
            {
                countemployee++;
            }
            ViewBag.countemployee = countemployee;
            var listuninvited = db.personal_historical.Where(x => x.sent_status == null && x.status_employee==1).ToList();
            int countuninvited = 0;
            foreach (var item in listuninvited)
            {
                countuninvited++;
            }
            ViewBag.countuninvited = countuninvited;
            var listinvited = db.historical_personal.Where(x => x.sent_status == 1 && x.status_employee==1 && x.access_token==1).ToList();
            int countinvited = 0;
            foreach (var item in listinvited)
            {
                countinvited++;
            }
            ViewBag.countinvited = countinvited;


            ViewBag.CheckValue = TempData["CekProblem"];
            var data = (from s in db.ms_personal_data where s.status_employee == 3 orderby s.NIP ascending select s).ToList();
            ViewBag.MsEmployee = data;
            return View();

        }

        public ActionResult IndexLogicTestProblems(string message)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }

            var check_duration_is_set_or_nah = db.ms_all_for_one.Where(x => x.pk_all_for_one == 1).FirstOrDefault();
            if (check_duration_is_set_or_nah == null)
            {
                ViewBag.InfoDuration = "Duration is not set.";
                ViewBag.CurrentDuration = " ";
                ViewBag.Char = " ";
            }
            else if (check_duration_is_set_or_nah.var_value == null)
            {
                ViewBag.InfoDuration = "Duration is not set.";
                ViewBag.CurrentDuration = " ";
                ViewBag.Char = " ";
            }
            else 
            {
                var str_seconds_duration = check_duration_is_set_or_nah.var_value;
                var seconds_duration = Int32.Parse(str_seconds_duration);
                var hours_duration = seconds_duration / 3600;
                var minute_modulo = (seconds_duration % 3600)/60;
                var curr_duration = hours_duration.ToString()+" Hours, "+ minute_modulo.ToString() + " Minute";

                ViewBag.InfoDuration = "Current Duration ";
                ViewBag.CurrentDuration = curr_duration;
                ViewBag.Char = ":";

            }
            //ms_employee obj_tbl_ms_employee = new ms_employee();
            //List<ms_employee> list_Tbl_Ms_Employees = new List<ms_employee>();
            //list_Tbl_Ms_Employees = db.ms_employee.Where(x => x.pk_ms_employee_id == 1).ToList();
            ViewBag.failedPDFmessage = message;
            return View(db.ms_logictest_problems.ToList());
        }

        public ActionResult MsEmployee()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data = (from s in db.ms_personal_data where s.status_employee == 3 orderby s.NIP ascending select s).ToList();
                ViewBag.MsEmployee = data;
                ViewBag.message = TempData["message"];
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(ms_personal_data _post, string position)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var Emailexist = (from s in db.ms_personal_data where s.email.Equals(_post.email) select s).ToList();
                        if (Emailexist.Count != 0)
                        {
                            TempData["message"] = 2;
                            return RedirectToAction("AddEmployee");
                        }
                        else
                        {
                            string add = "asad";
                            var d = add.Length;
                            var c = add.Substring(1);
                            var max = db.ms_personal_data.Max(r => r.NIP);

                            var angka = max.Substring(3, max.Length - 3);
                            _post.NIP = format_number(Convert.ToInt16(angka) + 1);
                            _post.status_employee = 3; //3 adalah status employee untuk karyawan 
                            _post.position = position;
                            _post.created_by = Session["UserName"].ToString();
                            _post.date_created = DateTime.Now;
                            db.ms_personal_data.Add(_post);
                            db.SaveChanges();
                            var pk = (from s in db.ms_personal_data where s.email.Equals(_post.email) select s.pk_employee).FirstOrDefault();
                            int pk2 = Convert.ToInt32(pk);
                            var recruit = new ms_recruit();
                            recruit.fk_applicant_information_form = pk2;
                            recruit.created_by = Session["UserName"].ToString();
                            recruit.data_crated = DateTime.Now;
                            db.ms_recruit.Add(recruit);
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 1;

                            return RedirectToAction("MsEmployee");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            
            return RedirectToAction("MsEmployee");
        }

        [HttpGet]
        public ActionResult DeleteEmployee(long id)
        {
            var pk_employee = (from s in db.ms_personal_data where s.pk_employee == id select s.pk_employee).FirstOrDefault();
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var query1 = db.tbl_user.Where(s => s.fk_employee == pk_employee).FirstOrDefault();
                    var query2 = db.ms_recruit.Where(s => s.fk_applicant_information_form == pk_employee).FirstOrDefault();
                    var query3 = db.historical_invitation_progress.Where(s => s.fk_applicant_information_form == pk_employee).ToList();
                    if (query1 != null || query2 != null || query3 != null)
                    {
                        db.tbl_user.Remove(query1);
                        //db.ms_recruit.Remove(query2);
                        db.SaveChanges();
                        foreach (var item in query3)
                        {
                            db.historical_invitation_progress.Remove(item);
                            db.SaveChanges();

                        }
                    }
                    var query = db.ms_personal_data.Where(s => s.pk_employee == id).FirstOrDefault();
                    db.ms_personal_data.Remove(query);
                    db.SaveChanges();
                    transaction.Commit();
                    TempData["message"] = 3;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("MsEmployee");
        }

        public ActionResult DetailEmployee(long id)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var query = (from s in db.ms_personal_data where s.pk_employee == id select s).ToList();
            ViewBag.pk_employee = id;
            var query3 = (from s in db.ms_emergency_family where s.fk_applicant_information_form == id select s).ToList();
            var query4 = (from s in db.ms_family_data where s.fk_applicant_information_form == id select s).ToList();
            var query5 = (from s in db.ms_favorite_job where s.fk_applicant_information_form == id select s).ToList();
            var query6 = (from s in db.ms_formal_education where s.fk_applicant_information_form == id select s).ToList();
            var query7 = (from s in db.ms_hobby where s.fk_applicant_information_form == id select s).ToList();
            var query8 = (from s in db.ms_informal_education where s.fk_applicant_information_form == id select s).ToList();
            var query9 = (from s in db.ms_language where s.fk_applicant_information_form == id select s).ToList();
            var query10 = (from s in db.ms_organization where s.fk_applicant_information_form == id select s).ToList();
            var query11 = (from s in db.ms_question where s.fk_employee == id select s).ToList();
            var query12 = (from s in db.ms_reference where s.fk_applicant_information_form == id select s).ToList();
            var query13 = (from s in db.ms_work_experience where s.fk_applicant_information_form == id select s).ToList();
            var query14 = (from s in db.ms_skill where s.fk_applicant_information_form == id select s).ToList();
            var query16 = (from s in db.ms_files where s.fk_applicant_information_form == id select s).ToList();
            var query15 = (from s in db.ms_recruit where s.fk_applicant_information_form == id select s).FirstOrDefault();
            ViewBag.Psychotest = query15.fk_psychotest_result;
            ViewBag.DISC = query15.fk_disc_result;
            ViewBag.Logictest = query15.fk_logictest_result;
            ViewBag.form = query15.fk_applicant_information_form;
            ViewBag.PersonalData = query;
            var result = DateTime.Now.Year;
            var year = new List<SelectType>();
            for (int i = Convert.ToInt16(result); i > 1990; i--)
            {
                year.Add(new SelectType() { Value = i.ToString(), Text = i.ToString() });
            }
            ViewBag.Year = year;

            ViewBag.MsEmployee = query;
            ViewBag.MsEmergency = query3;
            ViewBag.MsFamily = query4;
            ViewBag.FavoriteJob = query5;
            ViewBag.FormalEducation = query6;
            ViewBag.mshobby = query7;
            ViewBag.informaledu = query8;
            ViewBag.mslanguage = query9;
            ViewBag.msorganization = query10;
            ViewBag.msquestion = query11;
            ViewBag.msreference = query12;
            ViewBag.msworkexperience = query13;
            ViewBag.Files = query16;
            ViewBag.msskill = query14;
            ViewBag.message = TempData["message"];

            return View();
        }

        public ActionResult EditEmployee(long id)
        {
            if (Session["UserName"] != null)
            {
                RedirectToAction("Index");
            }
            var query = (from s in db.ms_personal_data where s.pk_employee == id select s).First();
            var query2 = (from s in db.ms_personal_data where s.pk_employee == id select s).ToList();
            ViewBag.MsEmployee = query2;
            ViewBag.Edit = 1;
            return View("DetailEmployee", query);
        }


        public ActionResult InviteFGD(string fullname, string email, int status, int retry, string place,string date,string start)
        {
            if (Session["UserName"] != null)
            {
                RedirectToAction("Index");
            }
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (status == 1 && retry == 1)
                    {
                        TempData["InviteFGD"] = 1;
                        TempData["fullnameFGD"] = fullname;
                        TempData["EmailFGD"] = email;
                        return RedirectToAction("ForumGroup");
                    }
                    else if (status == 1 && retry == 2) {
                        TempData["InviteFGD"] = 2;
                        TempData["fullnameFGD"] = fullname;
                        TempData["EmailFGD"] = email;
                        return RedirectToAction("ForumGroup");
                    }
                    else
                    {
                        var employee = db.ms_personal_data.Where(x => x.email == email).FirstOrDefault();
                        var query = db.ms_forum_discussion.Where(x => x.fk_employee == employee.pk_employee).FirstOrDefault();
                        if (query != null)
                        {
                                query.status_fgd = status;
                                db.Entry(query).State = EntityState.Modified;
                        }
                        else {
                            var validate = SendEmailfgd(fullname,place,date,start,email);
                            if (validate == true)
                            {
                                var fgd = new ms_forum_discussion();
                                fgd.fk_employee = employee.pk_employee;
                                fgd.place = place;
                                fgd.startdate_fgd = Convert.ToDateTime(date);
                                fgd.starttime_fgd = DateTime.ParseExact(start, "H:mm", null, System.Globalization.DateTimeStyles.None);
                                fgd.status_fgd = status;
                                db.ms_forum_discussion.Add(fgd);
                                TempData["message"] = 1;
                            }
                            else {
                                TempData["message"] = 2;
                            }

                        }
                        db.SaveChanges();
                        transaction.Commit();
                    }


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    TempData["message"] = 2;
                    transaction.Rollback();
                }
            }
            return RedirectToAction("ForumGroup");
        }

        public ActionResult ChangeStatus(string fullname, string email,int status,string position)
        {
            if (Session["UserName"] != null)
            {
                RedirectToAction("Index");
            }
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (status == 3 && position == null)
                    {
                        TempData["Changeemployee"] = 0;
                        TempData["Emailemployee"] = email;
                        TempData["Fullname"] = fullname;
                        return RedirectToAction("ApproveApplicant");
                    }
                    else if (status == 3 & position != "")
                    {
                        var validation = SendEmailStatus(fullname,email, status, position);
                        if (validation == true)
                        {
                            var max = db.ms_personal_data.Max(r => r.NIP);
                            var angka = max.Substring(3, max.Length - 3);
                            var query = db.ms_personal_data.Where(x => x.email == email).FirstOrDefault();
                            query.NIP = format_number(Convert.ToInt16(angka) + 1);
                            query.status_employee = Convert.ToInt16(status);
                            query.position = position;

                            db.Entry(query).State = EntityState.Modified;
                        }
                    }
                    else if (status == 2)
                    {
                        var validation = SendEmailStatus(fullname, email, status, position);
                        if (validation == true)
                        {
                            var query = db.ms_personal_data.Where(x => x.email == email).FirstOrDefault();
                            query.status_employee = status;
                            db.Entry(query).State = EntityState.Modified;
                        }
                    }
                    else if (status == 4)
                    {
                        var query = db.ms_personal_data.Where(x => x.email == email).FirstOrDefault();
                        query.status_employee = status;
                        db.Entry(query).State = EntityState.Modified;
                    }
                    else if (status == 1)
                    {
                        var query = db.ms_personal_data.Where(x => x.email == email).FirstOrDefault();
                        var query2 = db.historical_invitation_progress.Where(x => x.fk_applicant_information_form == query.pk_employee && x.access_token==1).FirstOrDefault();
                        query2.sent_status = null;
                        query2.access_token = null;
                        query2.starttime = null;
                        db.Entry(query2).State = EntityState.Modified;
                        db.SaveChanges();

                        query.status_employee = 1;
                        db.Entry(query).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Invite");
                    }
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return RedirectToAction("ApproveApplicant");
        }

        public static string format_number(int number)
        {
            string str_number = number.ToString();
            int n_zeros = 8 - str_number.Length;
            string allzeroz = "NDS00000";
            string resultzeros = allzeroz.Substring(0, n_zeros);
            string string_result = resultzeros + str_number;
            return string_result;
        }

        [HttpPost]
        public ActionResult EditEmployee(ms_personal_data _post)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var Emailexist = (from s in db.ms_personal_data where s.email.Equals(_post.email) select s).ToList();
                        if (Emailexist.Count > 1)
                        {
                            TempData["message"] = 2;
                            return RedirectToAction("DetailEmployee", new { id = _post.pk_employee });
                        }
                        else
                        {
                            var data1 = (from s in db.ms_recruit where s.fk_applicant_information_form == _post.pk_employee select s.pk_recruitID).FirstOrDefault();
                            if (data1 == 0)
                            {
                                var recruit = new ms_recruit();
                                recruit.fk_applicant_information_form = _post.pk_employee;
                                recruit.created_by = Session["UserName"].ToString();
                                recruit.data_crated = DateTime.Now;
                                db.ms_recruit.Add(recruit);
                                db.SaveChanges();
                            }
                            var data = db.ms_personal_data.Find(_post.pk_employee);
                            data.address = _post.address;
                            data.birth_date = _post.birth_date;
                            data.email = _post.email;
                            data.gender = _post.gender;
                            data.home_phone = _post.home_phone;
                            data.birth_place = _post.birth_place;
                            data.marriage_status = _post.marriage_status;
                            data.full_name = _post.full_name;
                            data.NIK = _post.NIK;
                            data.phone = _post.phone;
                            data.position = _post.position;
                            data.religion = _post.religion;
                            data.status_employee = 3;
                            data.created_by = Session["UserName"].ToString();
                            data.date_created = DateTime.Now;
                            db.Entry(data).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 1;
                            return RedirectToAction("DetailEmployee", new { id = _post.pk_employee });
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee");
        }

        public ActionResult MsApplicant()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data = (from s in db.personal_historical where s.status_employee==1   select s).ToList();
                ViewBag.MsApplicant = data;
                ViewBag.EditApplicant = TempData["Edit"];
                ViewBag.message = TempData["message"];
                ViewBag.NameApplicant = TempData["NameApplicant"];
                ViewBag.EmailApplicant = TempData["EmailApplicant"];
                ViewBag.LogictestResult = TempData["LogictestResult"];
                ViewBag.DiscResult = TempData["DiscResult"];
                ViewBag.PsychotestResult = TempData["PsychotestResult"];
                ViewBag.DetailApplicant = TempData["DetailApplicant"];
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddApplicant(ms_personal_data _post)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var Emailexist = (from s in db.ms_personal_data where s.email.Equals(_post.email) select s).ToList();
                        if (Emailexist.Count > 0)
                        {
                            var query = db.historical_personal.Where(x => x.email == _post.email).FirstOrDefault();
                            if (query.status_employee == 4)
                            {
                                TempData["NameApplicant"] = query.full_name;
                                TempData["EmailApplicant"] = query.email;
                                TempData["LogictestResult"] = query.fk_logictest_result;
                                TempData["DiscResult"] = query.fk_disc_result;
                                TempData["PsychotestResult"] = query.fk_psychotest_result;
                                TempData["DetailApplicant"] = query.fk_applicant_information_form;
                            }
                            else {
                                TempData["message"] = 2;
                            }
                            return RedirectToAction("MsApplicant");
                        }
                        else
                        {
                            _post.created_by = Session["UserName"].ToString();
                            _post.date_created = DateTime.Now;
                            _post.status_employee = 1; //1 adalah status employee untuk Applicant
                            db.ms_personal_data.Add(_post);
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 1;
                            return RedirectToAction("MsApplicant");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return RedirectToAction("MsApplicant");
        }

        [HttpGet]
        public ActionResult DeleteApplicant(long id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var query1 = db.ms_recruit.Where(s => s.fk_applicant_information_form == id).FirstOrDefault();
                    var query2 = db.historical_invitation_progress.Where(s => s.fk_applicant_information_form == id).ToList();
                    if (query1 != null)
                    {
                        TempData["message"] = 10;
                    }
                    else
                    {
                        foreach (var item in query2)
                        {
                            db.historical_invitation_progress.Remove(item);
                            db.SaveChanges();
                        }
                        var query = db.ms_personal_data.Where(s => s.pk_employee == id).First();
                        db.ms_personal_data.Remove(query);
                        db.SaveChanges();
                        transaction.Commit();
                        TempData["message"] = 3;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return RedirectToAction("MsApplicant");
        }

        public ActionResult EditApplicant(long id)
        {
            if (Session["UserName"] != null)
            {
                RedirectToAction("Index");
            }
            var query = (from s in db.ms_personal_data where s.pk_employee == id select s).ToList();
            TempData["Edit"] = query;
            return RedirectToAction("MsApplicant");
        }

        [HttpPost]
        public ActionResult EditApplicant(ms_personal_data _post, string link)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var Emailexist = (from s in db.ms_personal_data where s.email.Equals(_post.email) select s).ToList();
                        if (Emailexist.Count > 1)
                        {
                            TempData["message"] = 2;
                            return RedirectToAction("MsApplicant");
                        }
                        else
                        {
                            var data = db.ms_personal_data.Find(_post.pk_employee);
                            data.address = _post.address;
                            data.birth_date = _post.birth_date;
                            data.email = _post.email;
                            data.marriage_status = _post.marriage_status;
                            data.full_name = _post.full_name;
                            data.NIK = _post.NIK;
                            data.phone = _post.phone;
                            data.position = _post.position;
                            data.religion = _post.religion;
                            data.status_employee = 1;
                            data.created_by = Session["UserName"].ToString();
                            data.date_created = DateTime.Now;
                            db.Entry(data).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 4;
                            return RedirectToAction("MsApplicant");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("MsApplicant");
        }

        public static string md5(string key, string plainInput)
        {
            byte[] iv = new byte[16];
            byte[] array;
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainInput);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }



        public ActionResult Invite()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var check_value_testlogic = (db.ms_all_for_one.Where(x => x.pk_all_for_one == 1).FirstOrDefault()).var_value;
                if (check_value_testlogic == null) {
                    TempData["CekProblem"] = 1;
                    return RedirectToAction("Index");
                }
                var check_value_disc = (db.ms_all_for_one.Where(x => x.pk_all_for_one == 3).FirstOrDefault()).var_value;
                if (check_value_disc == null)
                {
                    TempData["CekProblem"] = 2;
                    return RedirectToAction("Index");
                }
                var problem_testlogic = db.ms_logictest_problems.ToList();
                if (problem_testlogic.Count == 0)
                {
                    TempData["CekProblem"] = 3;
                    return RedirectToAction("Index");
                }
                var problem_disc = db.ms_problem_disc.Where(x=>x.active==1).ToList();
                if (problem_disc.Count < 24)
                {
                    TempData["CekProblem"] = 4;
                    return RedirectToAction("Index");
                }
                var problem_psychotest = db.ms_psychotest_problems.Where(x => x.active == 1).ToList();
                var problem_psychotest1 = db.ms_psychotest_problems.Where(x => x.step == "Step 1").ToList();
                var problem_psychotest2 = db.ms_psychotest_problems.Where(x => x.step == "Step 2").ToList();
                var problem_psychotest3 = db.ms_psychotest_problems.Where(x => x.step == "Step 3").ToList();
                var problem_psychotest4 = db.ms_psychotest_problems.Where(x => x.step == "Step 4").ToList();
                var problem_psychotest5 = db.ms_psychotest_problems.Where(x => x.step == "Example Step 1").ToList();
                var problem_psychotest6 = db.ms_psychotest_problems.Where(x => x.step == "Example Step 2").ToList();
                var problem_psychotest7 = db.ms_psychotest_problems.Where(x => x.step == "Example Step 3").ToList();
                var problem_psychotest8 = db.ms_psychotest_problems.Where(x => x.step == "Example Step 4").ToList();

                if (problem_psychotest.Count < 53 ||
                    problem_psychotest1.Count < 12 ||
                    problem_psychotest2.Count < 14 ||
                    problem_psychotest3.Count < 13 ||
                    problem_psychotest4.Count < 10 ||
                    problem_psychotest5.Count < 1 ||
                    problem_psychotest6.Count < 1 ||
                    problem_psychotest7.Count < 1 ||
                    problem_psychotest8.Count < 1 )
                {
                    TempData["CekProblem"] = 5;
                    return RedirectToAction("Index");
                }

                var data = (from s in db.ms_personal_data where s.status_employee == 1 select s).ToList();
                //var data3 = (from s in db.ms_personal_data select s).ToList();
                //var data2 = (from s in db.ms_recruit select s).ToList();
                ViewBag.Inviteapplicant = data;
                //ViewBag.Inviteemployee = data3;
                //ViewBag.msrecruit = data2;
                var logictest = (from s in db.ms_logictest_problems select s).ToList();
                ViewBag.logictest = logictest;
                ViewBag.resent = TempData["resent"];
                ViewBag.soal = TempData["soal"];
                ViewBag.Emailapplicant = TempData["Emailapplicant"];
                ViewBag.message = TempData["message"];
                ViewBag.id = TempData["id"];
                ViewBag.boxname = TempData["boxname"];
                ViewBag.sentstatus = TempData["sentstatus"];
                ViewBag.startdate = TempData["startdate"];
                //ViewBag.DetailDISC = TempData["DetailDISC"];
                //ViewBag.NameDISC = TempData["NameDISC"];
                //ViewBag.status = TempData["status"];
                //if (ViewBag.status == 1)
                //{
                ViewBag.Inviteapplicant2 = TempData["Inviteapplicant2"];
                //}
                //else
                //{
                //    ViewBag.Inviteemployee2 = TempData["Inviteapplicant2"];
                //}
            }

            return View();
        }

        public ActionResult InviteEmployee()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data = (from s in db.ms_personal_data where s.status_employee == 3 select s).ToList();
                ViewBag.Inviteapplicant = data;
                var logictest = (from s in db.ms_logictest_problems select s).ToList();
                ViewBag.logictest = logictest;
                ViewBag.resent = TempData["resent"];
                ViewBag.soal = TempData["soal"];
                ViewBag.Emailapplicant = TempData["Emailapplicant"];
                ViewBag.EmailEmployee = TempData["EmailEmployee"];
                ViewBag.message = TempData["message"];
                ViewBag.id = TempData["id"];
                ViewBag.boxname = TempData["boxname"];
                ViewBag.sentstatus = TempData["sentstatus"];
                ViewBag.startdate = TempData["startdate"];
                ViewBag.Inviteapplicant2 = TempData["Inviteapplicant2"];
            }

            return View();
        }

        public ActionResult Progress()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.soal = TempData["soal"];
                ViewBag.resent = TempData["resent"];
                var logictest = (from s in db.ms_logictest_problems select s).ToList();
                ViewBag.logictest = logictest;
                ViewBag.starttime = TempData["starttime"];
                ViewBag.Emailapplicant = TempData["Emailapplicant"];
                ViewBag.Email = TempData["Email"];
                ViewBag.message = TempData["message"];
                ViewBag.FullName = TempData["FullName"];
                ViewBag.Inviteapplicant = (from s in db.historical_personal where s.disc_status+s.logictest_status+s.psychotest_status <9 && s.status_employee == 1 && s.access_token == 1 select s).ToList();

                ViewBag.solution = TempData["solution"];
                ViewBag.solution_path = TempData["solution_path"];
                ViewBag.score = TempData["score"];
                ViewBag.note = TempData["note"];
                ViewBag.fullname_logictest = TempData["fullname"];
                ViewBag.emaillogictest = TempData["emaillogictest"];
                ViewBag.statuslogictest = TempData["status"];
                ViewBag.problem = TempData["problem"];
                ViewBag.problem_path = TempData["problem_path"];

                ViewBag.Psychotestscore = TempData["score"];
                ViewBag.IQ = TempData["IQ"];

                ViewBag.discnotexist = TempData["discnotexist"];
                ViewBag.namedisc = TempData["namedisc"];

               ViewBag.FGDName = TempData["FGDName"];
               ViewBag.FGDEmail = TempData["FGDEmail"];
               ViewBag.FGDPlace = TempData["FGDPlace"];
               ViewBag.FGDDate = TempData["FGDDate"];
               ViewBag.FGDStart = TempData["FGDStart"];

            }

            return View();
        }


        public ActionResult ForumGroup()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Inviteapplicant = (from s in db.historical_personal where s.disc_status == 3 && s.logictest_status == 3 && s.psychotest_status == 3 && s.status_employee == 1 && s.access_token == 1  && (s.status_fgd !=2 || s.status_fgd == null) select s).ToList();
                ViewBag.InviteFGD = TempData["InviteFGD"];
                ViewBag.message = TempData["message"];
                ViewBag.fullnameFGD = TempData["fullnameFGD"];
                ViewBag.EmailFGD = TempData["EmailFGD"];

            }

            return View();
        }

        public ActionResult ApproveApplicant()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Inviteapplicant = (from s in db.historical_personal where s.disc_status == 3 && s.logictest_status == 3 && s.psychotest_status == 3 && s.status_employee == 1 && s.access_token == 1 && s.status_fgd == 2 select s).ToList();
                ViewBag.Changeemployee = TempData["Changeemployee"];
                ViewBag.EmailEmployee = TempData["Emailemployee"];
                ViewBag.Fullname = TempData["Fullname"];

            }

            return View();
        }

        public ActionResult Bootcamp()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Inviteapplicant = (from s in db.personal_historical where s.status_employee == 2 select s).ToList();
            }

            return View();
        }

        public ActionResult Failed()
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                ViewBag.Inviteapplicant = (from s in db.personal_historical where s.status_employee == 4 select s).ToList();
            }

            return View();
        }

        [HttpGet]
        public ActionResult DetailDISC(long id)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data = (from s in db.ms_disc_result where s.pk_disc_result == id select s).FirstOrDefault();
                var data2 = (from s in db.ms_disc_result where s.pk_disc_result == id select s).ToList();
                var pk = (from s in db.ms_disc_result where s.pk_disc_result == id select s.fk_applicant_information_form).FirstOrDefault();
                long pkd = Convert.ToInt64(pk);
                var data5 = (from s in db.ms_personal_data where s.pk_employee == pkd select s.full_name).FirstOrDefault();

                if (data.Total_M != 24) {
                    TempData["discnotexist"] = 1;
                    TempData["namedisc"] = data5;

                    return RedirectToAction("Progress");
                }


                int data3 = Convert.ToInt16((from s in db.ms_disc_graph_result where s.fk_disc_result == id select s.pk_disc_graph_result).FirstOrDefault());
                var data4 = (from s in db.ms_DISC_final_result where s.fk_disc_graph_result == data3 select s).ToList();
                ViewBag.FinalResult = data4;

                ViewBag.DataPoints1 = JsonConvert.SerializeObject(DotGraphMost(id));
                ViewBag.DataPoints2 = JsonConvert.SerializeObject(DotGraphLess(id));
                ViewBag.DataPoints3 = JsonConvert.SerializeObject(DotGraphChange(id));

                ViewBag.DetailDISC = data2;
                ViewBag.NameDISC = data5;

            }

            return View();
        }

        [HttpGet]
        public ActionResult DetailLogictest(long id)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var query = (from s in db.historical_personal where s.fk_logictest_result == id && s.logictest_score != null select s).FirstOrDefault();
                if (query != null)
                {
                    TempData["status"] = 1;
                    TempData["solution"] = query.logictest_solutions;
                    TempData["solution_path"] = query.logictest_solutions_path;
                    TempData["problem"] = query.problems_filename;
                    TempData["problem_path"] = query.problems_path;
                    TempData["score"] = query.logictest_score;
                    TempData["note"] = query.note;
                    TempData["fullname"] = query.full_name;
                    TempData["emailogictest"] = query.email;
                }
                else {
                    TempData["status"] = 2;
                    var query2 = (from s in db.historical_personal where s.fk_logictest_result == id select s).FirstOrDefault();
                    TempData["solution"] = query2.logictest_solutions;
                    TempData["solution_path"] = query2.logictest_solutions_path;
                    TempData["fullname"] = query2.full_name;
                    TempData["emailogictest"] = query2.email;
                }

            }

            return RedirectToAction("Progress");
        }

        [HttpGet]
        public ActionResult DetailFGD(long id)
        {
                var query = (from s in db.historical_personal where s.pk_forum_disccussion == id select s).FirstOrDefault();
                if (query != null)
                {
                    TempData["FGDName"] = query.full_name;
                    TempData["FGDEmail"] = query.email;
                    TempData["FGDPlace"] = query.place;
                    TempData["FGDDate"] = query.startdate_fgd;
                    TempData["FGDStart"] = query.starttime_fgd;
                }
                
            return RedirectToAction("Progress");
        }

        [HttpGet]
        public ActionResult DetailPsychotest(long id)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var score = (from s in db.ms_psychotest_result where s.pk_psychotest_result == id select s).FirstOrDefault();
                var IQ = (from s in db.ms_CFIT_to_IQ where s.pk_CFIT_to_IQ == score.fk_CFIT_to_IQ select s).FirstOrDefault();
                TempData["score"] = IQ.CFIT_IQ;
                TempData["IQ"] = score.score;
            }

            return RedirectToAction("Progress");
        }
        public List<DataPoint> DotGraphMost(long id)
        {
            var obj_graph = db.ms_disc_graph_result.Where(x => x.fk_disc_result == id).FirstOrDefault();
            double md = Convert.ToDouble(obj_graph.RGMD);
            double mi = Convert.ToDouble(obj_graph.RGMI);
            double ms = Convert.ToDouble(obj_graph.RGMS);
            double mc = Convert.ToDouble(obj_graph.RGMC);
            List<DataPoint> dataPoints1 = new List<DataPoint>();
            dataPoints1.Add(new DataPoint("D", md));
            dataPoints1.Add(new DataPoint("I", mi));
            dataPoints1.Add(new DataPoint("S", ms));
            dataPoints1.Add(new DataPoint("C", mc));

            return dataPoints1;
        }

        public List<DataPoint> DotGraphLess(long id)
        {
            var obj_graph = db.ms_disc_graph_result.Where(x => x.fk_disc_result == id).FirstOrDefault();
            double ld = Convert.ToDouble(obj_graph.RGLD);
            double li = Convert.ToDouble(obj_graph.RGLI);
            double ls = Convert.ToDouble(obj_graph.RGLS);
            double lc = Convert.ToDouble(obj_graph.RGLC);
            List<DataPoint> dataPoints1 = new List<DataPoint>();
            dataPoints1.Add(new DataPoint("D", ld));
            dataPoints1.Add(new DataPoint("I", li));
            dataPoints1.Add(new DataPoint("S", ls));
            dataPoints1.Add(new DataPoint("C", lc));

            return dataPoints1;
        }

        public List<DataPoint> DotGraphChange(long id)
        {
            var obj_graph = db.ms_disc_graph_result.Where(x => x.fk_disc_result == id).FirstOrDefault();
            double cd = Convert.ToDouble(obj_graph.RGAD);
            double ci = Convert.ToDouble(obj_graph.RGAI);
            double cs = Convert.ToDouble(obj_graph.RGAS);
            double cc = Convert.ToDouble(obj_graph.RGAC);
            List<DataPoint> dataPoints1 = new List<DataPoint>();
            dataPoints1.Add(new DataPoint("D", cd));
            dataPoints1.Add(new DataPoint("I", ci));
            dataPoints1.Add(new DataPoint("S", cs));
            dataPoints1.Add(new DataPoint("C", cc));

            return dataPoints1;
        }



        [HttpPost]
        public ActionResult DetailReceiver(string item)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            String item2 = item.Replace(" ", "");
            String[] name = item2.Split('-');
            String name_applicant = name[0];
            var email_applicant = name[1];
            var status = Convert.ToInt16(name[2]);
            var data2 = (from s in db.personal_historical where s.email.Equals(email_applicant) select s).ToList();
            TempData["Inviteapplicant2"] = data2;
            TempData["boxname"] = item;
            var query = (from s in db.ms_personal_data where s.email.Equals(email_applicant) select s.pk_employee).FirstOrDefault();
            int pk = Convert.ToInt16(query);
            var sentstatus = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == pk select s).FirstOrDefault();
            if (sentstatus != null) {
                TempData["sentstatus"] = sentstatus.sent_status;
                TempData["startdate"] = sentstatus.starttime;
            }
            var detail = db.ms_personal_data.Where(x=>x.pk_employee==pk).FirstOrDefault();
            if (detail.status_employee ==3 ) {
                return RedirectToAction("InviteEmployee");
            }
            //foreach (ms_personal_data x in data2)
            //{
            //    var PK_applicant = Convert.ToInt64(x.pk_employee);
            //    var data3 = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == PK_applicant && s.access_token == 1 select s).ToList();
            //    if (data3 == null)
            //    {
            //        string ket = "empty";
            //        TempData["Progress"] = ket;
            //    }
            //    else
            //    {
            //        TempData["Progress"] = data3;
            //    }
            //}

            return RedirectToAction("Invite");
        }

        public bool SendEmail(string email, string obj, string start, string end)
        {
            var to_fullname = (db.ms_personal_data.Where(x => x.email == email).FirstOrDefault()).full_name;
                MailMessage mm = new MailMessage("prewtry7@gmail.com", email);
                mm.From = new MailAddress("prewtry7@gmail.com", "Nawadaya");
                mm.Subject = "Assessments Invitation";
            //string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + to_send + ",</b><br>Thanks for your application at PT. Nawadata Solutions.<br>Please take these assessments by clicking on the link below.<br><br><br> <a class='button' href='http://localhost/Nawadaya2/Recruitment/index?obj=" + obj + "'>Link to Assessments Page</a><br><br>Thanks<br><br>Regard,<br><br>Nawadata Solutions.";
            //
            //var obj_ADM = db.logins.Where(x => x.full_name == Session["FullName"]).FirstOrDefault();
            //var ADM_full_name = (obj_ADM).full_name;
            //var ADM_email = (obj_ADM).email;
            DateTime datestart = Convert.ToDateTime(start);
            DateTime datexpired = Convert.ToDateTime(end);
            var assess_start = datestart.ToString("dddd, dd MMMM yyyy");
            var assess_expired = datexpired.ToString("dddd, dd MMMM yyyy");

            string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + to_fullname + ",</b><br>Thanks for your application at PT. Nawadata Solutions.<br>Please take these assessments by clicking on the link below.<br><b>(You can only access this link from " + assess_start + " to " + assess_expired + " )</b><br><br> <a class='button' href='http://localhost/Nawadaya2/Recruitment/index?obj=" + obj + "'>Link to Assessments Page</a><br><br>Best Regards,<br>" + Session["FullName"] + "<br>" + "Nawadaya Admin<br><br><b>PT Nawa Data Solutions</b><br>Prince Center Building Lt. 06 Unit 605<br>Jl Jend Sudirman KAV 3 - 4, Karet Tengsin Tanah Abang<br>Jakarta Pusat 10250";

            //
            mm.Body = tulisan;
                mm.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;

                NetworkCredential nc = new NetworkCredential("prewtry7@gmail.com", "husehusen");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = nc;
                try
                {
                    smtp.Send(mm);
                }
                catch (Exception ex)
                {
                Console.WriteLine(ex.Message);
                return false;
                }

            return true;

        }

        public bool SendEmailfgd(string fullname, string place, string date, string time, string email)
        {
            var date2 = Convert.ToDateTime(date);
            MailMessage mm = new MailMessage("prewtry7@gmail.com", email);
            mm.From = new MailAddress("prewtry7@gmail.com", "Nawadaya");
            mm.Subject = "Group Interview Invitation";
            string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + fullname + ",</b><br>PT.Nawa Data Solutions invite you to Forum Group Discussion at:<br><br>"+date2.ToString("ddd, MMM d \"'\"yy")+ "<br><br>"+time+" - Finished <br><br>PIC :"+ Session["FullName"] + "<br><br>Address : "+place+"<br><br><br>If unable to attend or late, please keep us informed in advance by replying to this email or WA to me at number 081281915322<br><br>Thank you.";

            mm.Body = tulisan;
            mm.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;

            NetworkCredential nc = new NetworkCredential("prewtry7@gmail.com", "husehusen");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = nc;
            try
            {
                smtp.Send(mm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;

        }

        public bool SendEmailStatus(string fullname, string email, int status, string position)
        {
            MailMessage mm = new MailMessage("prewtry7@gmail.com", email);
            mm.From = new MailAddress("prewtry7@gmail.com", "Nawadaya");
            mm.Subject = "Congratulation";
            string tulisan = "";
            if (status == 2)
            {
                tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + fullname + ",</b><br>Congratulation.<br>You has accepted as bootcamp<br><br>Thanks<br><br>Regard,<br><br>Nawadata Solutions.";
            }
            else {
                tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + fullname + ",</b><br>Congratulation.<br>You has accepted as Empolyee and your position is "+position+"<br><br>Thanks<br><br>Regard,<br><br>Nawadata Solutions.";
            }

            mm.Body = tulisan;
            mm.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;

            NetworkCredential nc = new NetworkCredential("prewtry7@gmail.com", "husehusen");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = nc;
            try
            {
                smtp.Send(mm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

            return true;

        }


        public ActionResult Choosesoal(string email, int status)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var data = (from s in db.ms_problem select s).ToList();
            var query = (from s in db.ms_personal_data where s.email.Equals(email) select s).First();
            var data2 = (from s in db.ms_personal_data where s.email.Equals(email) select s).ToList();
            var name = (from s in db.ms_personal_data where s.email.Equals(email) select s.full_name).ToList();
            
                TempData["Inviteapplicant2"] = data2;
                TempData["soal"] = data;
                TempData["Emailapplicant"] = email;
                string id = name + " - " + email;
                TempData["id"] = id;

            if (query.status_employee == 3) {
                var data3 = (from s in db.personal_historical where s.email.Equals(email) select s).ToList();
                TempData["Inviteapplicant2"] = data3;
                if (status == 1)
                {
                    TempData["resent"] = 1;
                    TempData["Emailapplicant"] = null;
                    TempData["EmailEmployee"] = email;
                    return RedirectToAction("InviteEmployee", query);
                }
                return RedirectToAction("InviteEmployee", query);
                
            }
            if (status == 1)
            {
                TempData["resent"] = 1;
                return RedirectToAction("Progress", query);
            }
            else {
                return RedirectToAction("Invite", query);
            }
        }

        public ActionResult ConfirmResent(string email)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var query = (from s in db.historical_personal where s.email == email && s.access_token == 1 select s).FirstOrDefault();
            TempData["Email"] = query.email;
            TempData["FullName"] = query.full_name;
            TempData["starttime"] = query.starttime;

            return RedirectToAction("Progress");

        }



        public static string DecryptString(string key, string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

        [HttpPost]
        public ActionResult Invite2(ms_personal_data obj, string problem, string logictestproblem,string start,string end)
        {

            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (Session["UserName"] == null)
                    {
                        return RedirectToAction("Index", "Login");
                    }
                    else
                    {
                        var fk_applicant = (from s in db.ms_personal_data where s.email.Equals(obj.email) select s.pk_employee).FirstOrDefault();
                        var historicalexist = db.historical_invitation_progress.Where(s => s.fk_applicant_information_form == fk_applicant).FirstOrDefault();
                        var query2 = db.ms_personal_data.Where(s => s.email == obj.email).FirstOrDefault();
                        string[] arrayproblem = problem.Split(',');
                        var key = "b14ca5898a4e4133bbce2ea2315a1916";
                        var link = string.Concat(md5(key, string.Format("obj={1}&email={0}", obj.email.ToString(), problem.ToString())));
                        var decrptedInput = DecryptString(key, link.Substring(link.IndexOf("=") + 1));
                        var validation = SendEmail(obj.email, link, start, end);
                        if (validation == true)
                        {
                            var query = new historical_invitation_progress();
                            query.fk_applicant_information_form = query2.pk_employee;
                            query.applicant_information_status = 0;
                            for (int i = 0; i < arrayproblem.Length - 1; i++)
                            {
                                int code = Convert.ToInt32(arrayproblem[i]);
                                if (code == 2)
                                {
                                    query.disc_status = 0;
                                }
                                else if (code == 3)
                                {
                                    query.psychotest_status = 0;
                                }
                                else
                                {
                                    query.logictest_status = 0;
                                }
                            }
                            query.sent_status = 1;
                            query.access_token = 1;
                            DateTime datestart = Convert.ToDateTime(start);
                            DateTime datexpired = Convert.ToDateTime(end);
                            query.starttime = datestart;
                            query.expiredtime = datexpired;
                            query.link = link;
                            query.fk_logictest_problems = Convert.ToInt16(logictestproblem);
                            query.created_by = Session["UserName"].ToString();
                            query.data_crated = DateTime.Now;
                            db.historical_invitation_progress.Add(query);
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 1;
                            if (query2.status_employee == 3)
                            {
                                return RedirectToAction("InviteEmployee");

                            }
                        }
                        else
                        {
                            TempData["message"] = 2;
                            var employee = db.ms_personal_data.Where(x => x.email == obj.email).FirstOrDefault();
                            if (employee.status_employee == 3)
                            {
                                return RedirectToAction("InviteEmployee");

                            }
                            return RedirectToAction("Invite");
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("progress");
        }


        [HttpPost]
        public ActionResult ResentEmail(ms_personal_data obj, string problem, string logictestproblem, string start, string end)
        {
            var fk_applicant = (from s in db.ms_personal_data where s.email.Equals(obj.email) select s.pk_employee).FirstOrDefault();
            var email = obj.email.ToString();
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var logictest = logictestproblem;
                    var starttime = start;
                    var endtime = end;
                    string[] arrayproblem = problem.Split(',');
                    var key = "b14ca5898a4e4133bbce2ea2315a1916";
                    var link = string.Concat(md5(key, string.Format("obj={1}&email={0}", email.ToString(), problem.ToString())));
                    var validation = SendEmail(obj.email, link, start, end);
                    if (validation == true)
                    {
                        var decrptedInput = DecryptString(key, link.Substring(link.IndexOf("=") + 1));
                        var max = db.historical_invitation_progress.Where(o => o.fk_applicant_information_form == fk_applicant)
                                   .Max(o => o.pk_historical_invitation_progress);
                        var before = db.historical_invitation_progress.Where(o => o.fk_applicant_information_form == fk_applicant)
                                   .ToList();
                        var x = db.historical_invitation_progress.Find(max);
                        foreach (historical_invitation_progress i in before)
                        {
                            x.fk_applicant_information_form = fk_applicant;
                            x.applicant_information_status = i.applicant_information_status;
                            x.disc_status = i.disc_status;
                            x.psychotest_status = i.psychotest_status;
                            x.logictest_status = i.logictest_status;
                            x.created_by = i.created_by;
                            x.data_crated = i.data_crated;
                            x.link = i.link;
                        }
                        x.access_token = 0;
                        db.Entry(x).State = EntityState.Modified;
                        var query = new historical_invitation_progress();
                        var query2 = db.ms_personal_data.Where(s => s.email == email).FirstOrDefault();
                        query.fk_applicant_information_form = query2.pk_employee;
                        var query3 = db.ms_recruit.Where(s => s.fk_applicant_information_form == query2.pk_employee).FirstOrDefault();
                        if (query3 != null)
                        {
                            query.applicant_information_status = 3;
                        }
                        else
                        {
                            query.applicant_information_status = 0;
                        }
                        for (int i = 0; i < arrayproblem.Length - 1; i++)
                        {
                            int code = Convert.ToInt32(arrayproblem[i]);
                            if (code == 2)
                            {
                                query.disc_status = 0;
                            }
                            else if (code == 3)
                            {
                                query.psychotest_status = 0;
                            }
                            else
                            {
                                query.logictest_status = 0;
                            }
                        }
                        query.sent_status = 1;
                        query.access_token = 1;
                        DateTime datestart = Convert.ToDateTime(starttime);
                        DateTime datexpired = Convert.ToDateTime(endtime);
                        query.starttime = datestart;
                        query.expiredtime = datexpired;
                        query.fk_logictest_problems = Convert.ToInt16(logictestproblem);
                        query.link = link;
                        query.created_by = Session["UserName"].ToString();
                        query.data_crated = DateTime.Now;
                        db.historical_invitation_progress.Add(query);
                        db.SaveChanges();
                        transaction.Commit();
                        TempData["message"] = 3;
                        if (query2.status_employee == 3)
                        {
                            return RedirectToAction("InviteEmployee");

                        }
                        return RedirectToAction("progress");
                    }
                    else
                    {
                        TempData["message"] = 2;
                        var employee = db.ms_personal_data.Where(x => x.email == obj.email).FirstOrDefault();
                        if (employee.status_employee == 3)
                        {
                            return RedirectToAction("InviteEmployee");

                        }
                        return RedirectToAction("Invite");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return RedirectToAction("Invite");
        }

        public ActionResult MsDISC()
        {

            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data = (from s in db.ms_problem_disc select s).ToList();
                ViewBag.AddDISC = data;
                ViewBag.message = TempData["message"];
            }
            //Start/DISC duration/ editedbykms
            var check_DISCduration_is_set_or_nah = db.ms_all_for_one.Where(x => x.pk_all_for_one == 3).FirstOrDefault();
            if (check_DISCduration_is_set_or_nah == null)
            {
                ViewBag.DISCInfoDuration = "Duration is not set.";
                ViewBag.DISCCurrentDuration = " ";
                ViewBag.DISCChar = " ";
            }
            else if (check_DISCduration_is_set_or_nah.var_value == null)
            {
                ViewBag.DISCInfoDuration = "Duration is not set.";
                ViewBag.DISCCurrentDuration = " ";
                ViewBag.DISCChar = " ";
            }
            else
            {
                var DISC_str_seconds_duration = check_DISCduration_is_set_or_nah.var_value;
                var DISC_seconds_duration = Int32.Parse(DISC_str_seconds_duration);
                var DISC_hours_duration = DISC_seconds_duration / 3600;
                var DISC_minute_modulo = (DISC_seconds_duration % 3600) / 60;
                var DISC_curr_duration = DISC_hours_duration.ToString() + " Hours, " + DISC_minute_modulo.ToString() + " Minute";

                ViewBag.DISCInfoDuration = "Current Duration ";
                ViewBag.DISCCurrentDuration = DISC_curr_duration;
                ViewBag.DISCChar = ":";

            }
            //

            return View();
        }

        [HttpPost]
        public ActionResult AddDISC(ms_problem_disc _post)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        _post.active = 0;
                        _post.created_by = Session["UserName"].ToString();
                        _post.data_crated = DateTime.Now;
                        db.ms_problem_disc.Add(_post);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("MsDISC");

                        //return Json(_post);
                    }
                    TempData["message"] = 0;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult DeleteDISC(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var query = db.ms_problem_disc.Where(s => s.pk_problem_disc == id).First();
                    db.ms_problem_disc.Remove(query);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return RedirectToAction("MsDISC");
        }

        [HttpGet]
        public ActionResult DeletePsychotest(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var query = db.ms_psychotest_problems.Where(s => s.pk_psychotest_problems == id).First();
                    db.ms_psychotest_problems.Remove(query);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return RedirectToAction("MsPsychotest");
        }

        public ActionResult EditDISC(int id)
        {
            var query = db.ms_problem_disc.Where(s => s.pk_problem_disc == id).First();
            ViewBag.EditDISC = query;
            var data = (from s in db.ms_problem_disc select s).ToList();
            ViewBag.AddDISC = data;
            ViewBag.PKDISC = id;
            return View("MsDISC", query);
        }

        public ActionResult ShowDISC(int id)
        {
            var query = db.ms_problem_disc.Where(s => s.pk_problem_disc == id).First();
            ViewBag.EditDISC = query;
            var data = (from s in db.ms_problem_disc select s).ToList();
            ViewBag.AddDISC = data;
            ViewBag.PKDISC = id;
            return RedirectToAction("MsDISC");
        }

        public ActionResult ActiveDISC(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var count = db.ms_problem_disc.Where(x => x.active == 1).ToList();
                    var data = db.ms_problem_disc.Find(id);

                    int i = 0;
                    foreach (var a in count)
                    {
                        i++;
                    }
                    if (i >= 24)
                    {
                        TempData["message"] = 1;
                        return RedirectToAction("MsDISC");
                    }
                    else
                    {
                        if (data.active != null)
                        {
                            if (data.active == 0)
                            {
                                data.active = 1;

                            }
                            else
                            {
                                data.active = 0;
                            }
                        }
                    }
                    data.created_by = Session["UserName"].ToString();
                    data.data_crated = DateTime.Now;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("MsDISC");
        }

        public ActionResult ActivePsychotest(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var data = db.ms_psychotest_problems.Find(id);
                    var count = db.ms_psychotest_problems.Where(x => x.active == 1 && x.step == data.step).ToList();
                    

                    if (data.step == "Step 1") {
                        int i = 0;
                        foreach (var a in count)
                        {
                            i++;
                        }
                        if (i >= 12)
                        {
                            TempData["message"] = 1;
                            return RedirectToAction("MsPsychotest");
                        }
                        else
                        {

                                if (data.active == 1)
                                {
                                    data.active = 0;

                                }
                                else
                                {
                                    data.active = 1;
                                }
                            
                        }
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();

                    }
                    else if (data.step == "Step 2")
                    {
                        int i = 0;
                        foreach (var a in count)
                        {
                            i++;
                        }
                        if (i >= 14)
                        {
                            TempData["message"] = 2;
                            return RedirectToAction("MsPsychotest");
                        }
                        else
                        {
                            if (data.active == 1)
                            {
                                data.active = 0;

                            }
                            else
                            {
                                data.active = 1;
                            }
                        }
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();

                    }
                    else if (data.step == "Step 3")
                    {
                        int i = 0;
                        foreach (var a in count)
                        {
                            i++;
                        }
                        if (i >= 13)
                        {
                            TempData["message"] = 3;
                            return RedirectToAction("MsPsychotest");
                        }
                        else
                        {
                            if (data.active == 1)
                            {
                                data.active = 0;

                            }
                            else
                            {
                                data.active = 1;
                            }
                        }
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();

                    }
                    else if (data.step == "Step 4")
                    {
                        int i = 0;
                        foreach (var a in count)
                        {
                            i++;
                        }
                        if (i >= 10)
                        {
                            TempData["message"] = 4;
                            return RedirectToAction("MsPsychotest");
                        }
                        else
                        {
                            if (data.active == 1)
                            {
                                data.active = 0;

                            }
                            else
                            {
                                data.active = 1;
                            }
                        }
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();

                    }
                    else if (data.step == "Example Step 1")
                    {
                        int i = 0;
                        foreach (var a in count)
                        {
                            i++;
                        }
                        if (i >= 1)
                        {
                            TempData["message"] = 5;
                            return RedirectToAction("MsPsychotest");
                        }
                        else
                        {
                            if (data.active == 1)
                            {
                                data.active = 0;

                            }
                            else
                            {
                                data.active = 1;
                            }
                        }
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();

                    }
                    else if (data.step == "Example Step 2")
                    {
                        int i = 0;
                        foreach (var a in count)
                        {
                            i++;
                        }
                        if (i >= 1)
                        {
                            TempData["message"] = 6;
                            return RedirectToAction("MsPsychotest");
                        }
                        else
                        {
                            if (data.active == 1)
                            {
                                data.active = 0;

                            }
                            else
                            {
                                data.active = 1;
                            }
                        }
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    else if (data.step == "Example Step 3")
                    {
                        int i = 0;
                        foreach (var a in count)
                        {
                            i++;
                        }
                        if (i >= 1)
                        {
                            TempData["message"] = 7;
                            return RedirectToAction("MsPsychotest");
                        }
                        else
                        {
                            if (data.active == 1)
                            {
                                data.active = 0;

                            }
                            else
                            {
                                data.active = 1;
                            }
                        }
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    else if (data.step == "Example Step 4")
                    {
                        int i = 0;
                        foreach (var a in count)
                        {
                            i++;
                        }
                        if (i >= 1)
                        {
                            TempData["message"] = 8;
                            return RedirectToAction("MsPsychotest");
                        }
                        else
                        {
                            if (data.active == 1)
                            {
                                data.active = 0;

                            }
                            else
                            {
                                data.active = 1;
                            }
                        }
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("MsPsychotest");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDISC(ms_problem_disc item)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var data = db.ms_problem_disc.Find(item.pk_problem_disc);
                        data.statment1 = item.statment1;
                        data.statment2 = item.statment2;
                        data.statment3 = item.statment3;
                        data.statment4 = item.statment4;
                        data.M1 = item.M1;
                        data.M2 = item.M2;
                        data.M3 = item.M3;
                        data.M4 = item.M4;
                        data.L1 = item.L1;
                        data.L2 = item.L2;
                        data.L3 = item.L3;
                        data.L4 = item.L4;
                        data.created_by = Session["UserName"].ToString();
                        data.data_crated = DateTime.Now;
                        db.Entry(data).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        // return Json(_post);
                        return RedirectToAction("MsDISC");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return View();
        }

        public ActionResult MsPsychotest()
        {

            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data = db.ms_psychotest_problems;
                ViewBag.MsPshycotest = data;
                ViewBag.message = TempData["message"];
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddPsychotest(ms_psychotest_problems obj, HttpPostedFileBase file1, HttpPostedFileBase file2, string step,string answer2)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                 {
                    if (step == "Example Step 2" || step == "Step 2") {
                        string _FileName3 = Path.GetFileName(file1.FileName);
                        string _path3 = Path.Combine(Server.MapPath("~/PshycotestProblems"), _FileName3);
                        //string _FileExt = ;
                        file1.SaveAs(_path3);
                        obj.problem_path = _FileName3;
                        obj.answer = answer2;
                        obj.step = step;
                        //obj_pdf.active_problems = 0;
                        db.ms_psychotest_problems.Add(obj);
                        db.SaveChanges();
                        transaction.Commit();
                        //return View();
                        return RedirectToAction("MsPsychotest");

                    }
                    string _FileName1 = Path.GetFileName(file1.FileName);
                    string _path = Path.Combine(Server.MapPath("~/PshycotestProblems"), _FileName1);
                    //string _FileExt = ;
                    file1.SaveAs(_path);
                    string _FileName2 = Path.GetFileName(file2.FileName);
                    string _path2 = Path.Combine(Server.MapPath("~/PshycotestProblems"), _FileName2);
                    //string _FileExt = ;
                    file2.SaveAs(_path2);


                    obj.problem_path = _FileName1;
                    obj.optional_path = _FileName2;
                    obj.step = step;
                    obj.active = 0;
                    //obj_pdf.active_problems = 0;
                    db.ms_psychotest_problems.Add(obj);
                    db.SaveChanges();
                    transaction.Commit();
                    //return View();
                    return RedirectToAction("MsPsychotest");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return RedirectToAction("MsPsychotest");
                }

            }

        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult AddLogicTestPDF(ms_logictest_problems obj_pdf, HttpPostedFileBase file)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    //HttpPostedFileBase file = new HttpPostedFileBase();
                    if (file.ContentLength > 0 && Path.GetExtension(file.FileName).ToUpper() == ".PDF" && Path.GetFileName(file.FileName).Length < 105)
                    {

                        string _FileName = Path.GetFileName(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/UploadedLogicTestProblemsPDF"), _FileName);
                        //string _FileExt = ;
                        


                        obj_pdf.problems_filename = _FileName;
                        obj_pdf.problems_path = "~/UploadedLogicTestProblemsPDF";
                        //////
                        var list_checksamenamePDF = db.ms_logictest_problems.Where(x => x.problems_filename == _FileName).ToList();
                        var count_exstgPDF = list_checksamenamePDF.Count;
                        
                        if (count_exstgPDF != 0) 
                        {
                            //transaction.Rollback();
                            ViewBag.failedMessage = "3";
                            return RedirectToAction("IndexLogicTestProblems", new { message = (string)ViewBag.failedMessage });
                        }
                        
                        //////
                        //obj_pdf.active_problems = 0;
                        db.ms_logictest_problems.Add(obj_pdf);
                        db.SaveChanges();
                        transaction.Commit();
                        file.SaveAs(_path);
                        //ViewBag.failedMessage = "File Uploaded Successfully!!";
                        ViewBag.failedMessage = "0";
                        //return View();
                        return RedirectToAction("IndexLogicTestProblems", new { message = (string)ViewBag.failedMessage });
                    }
                    else if (Path.GetFileName(file.FileName).Length >= 105)
                    {
                        ViewBag.failedMessage = "1";
                        return RedirectToAction("IndexLogicTestProblems", new { message = (string)ViewBag.failedMessage });
                    }
                    else
                    {
                        //ViewBag.FileStatus = "Invalid file format.";
                        ViewBag.failedMessage = "2";
                        return RedirectToAction("IndexLogicTestProblems", new {message= (string)ViewBag.failedMessage });

                    }
                    //return View();
                    //return RedirectToAction("IndexLogicTestProblems");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    

                        Console.WriteLine(ex.Message);
                    return RedirectToAction("IndexLogicTestProblems");
                }

            }

        }

        public ActionResult HistoricalApplicantInvitation()
        {
            ViewBag.HistoricalEmployee = (from s in db.personal_historical where s.starttime !=null select s).ToList();
            return View();
        }

        public ActionResult HistoricalEmployeeInvitation()
        {
            ViewBag.HistoricalEmployee = (from s in db.historical_personal where s.status_employee == 3 select s).ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateLogicTestDuration(string minutes)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var set_minutes = Int32.Parse(minutes) * 60;
                    var str_set_seconds = set_minutes.ToString();

                    var check_duration_obj_is_missing_or_nah = db.ms_all_for_one.Where(x => x.pk_all_for_one == 1).FirstOrDefault();
                    if (check_duration_obj_is_missing_or_nah == null)
                    {
                        transaction.Rollback();
                        ViewBag.DurationMessage = "Logic Test Duration Object is Missing.";
                        return RedirectToAction("IndexLogicTestProblems");
                    }
                    else 
                    {
                        check_duration_obj_is_missing_or_nah.var_value= str_set_seconds;
                        check_duration_obj_is_missing_or_nah.updated_by = Session["Email"].ToString();
                        check_duration_obj_is_missing_or_nah.date_updated = DateTime.Now;
                        db.Entry(check_duration_obj_is_missing_or_nah).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("IndexLogicTestProblems");
                    }
 
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                    return View();
                }

            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateDISCTestDuration(string minutes)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var set_minutes = Int32.Parse(minutes) * 60;
                    var str_set_seconds = set_minutes.ToString();

                    //pk=3
                    var check_duration_obj_is_missing_or_nah = db.ms_all_for_one.Where(x => x.pk_all_for_one == 3).FirstOrDefault();
                    if (check_duration_obj_is_missing_or_nah == null)
                    {
                        transaction.Rollback();
                        ViewBag.DISCDurationMessage = "Logic Test Duration Object is Missing.";
                        return RedirectToAction("MsDISC");
                    }
                    else
                    {
                        check_duration_obj_is_missing_or_nah.var_value = str_set_seconds;
                        check_duration_obj_is_missing_or_nah.updated_by = Session["Email"].ToString();
                        check_duration_obj_is_missing_or_nah.date_updated = DateTime.Now;
                        db.Entry(check_duration_obj_is_missing_or_nah).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("MsDISC");
                    }

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                    return View();
                }

            }

        }


        public ActionResult ReportTotalApplicant(string ReportName, string ReportDescription, int Width, int Height)
        {
            var rptInfo = new ReportInfo
            {
                ReportName = ReportName,
                ReportDescription = ReportDescription,
                ReportURL = String.Format("../../Reports/ReportForm.aspx?ReportName={0}&Height={1}", ReportName, Height),
                Width = Width,
                Height = Height

            };

            return View(rptInfo);
        }

        public ActionResult ReportTotalApplicantRecruit(string ssrsname)
        {
            var ssrsURL = ConfigurationManager.AppSettings["SSRSReportUrl"].ToString();
            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Remote;
            viewer.SizeToReportContent = true;
            viewer.AsyncRendering = true;
            viewer.ServerReport.ReportServerUrl = new Uri(ssrsURL);
            viewer.ServerReport.ReportPath = "/Tes SSRS/" + ssrsname;
            viewer.Width=Unit.Percentage(100);
            viewer.Height= Unit.Percentage(100);
            viewer.ZoomMode = ZoomMode.FullPage;

            ViewBag.ReportViewer = viewer;


            return View();
        }

        [HttpPost]
        public ActionResult AddQustion(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                            var question = new ms_question();
                            question = _post.pertanyaan;
                            question.fk_employee = pk;
                            db.ms_question.Add(question);
                            db.SaveChanges();
                            transaction.Commit();

                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee",new { id=pk});
        }

        [HttpPost]
        public ActionResult AddReference(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.referensi) {
                            var reference = new ms_reference();
                            if (item.nama !=null) {
                                reference = item;
                                reference.fk_applicant_information_form = pk;
                                db.ms_reference.Add(reference);
                            }
 
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddFamilyEmergency(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.keluarga_darurat)
                        {
                            var emergency = new ms_emergency_family();
                            if (item.nama !=null)
                            {
                                emergency = item;
                                emergency.fk_applicant_information_form = pk;
                                db.ms_emergency_family.Add(emergency);

                            }
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        

        [HttpPost]
        public ActionResult AddFavoriteJob(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.pekerjaan_favorite)
                        {
                            var favjob = new ms_favorite_job();
                            if (item.nama !=null) {
                                favjob = item;
                                favjob.fk_applicant_information_form = pk;
                                db.ms_favorite_job.Add(favjob);
                            }
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddHobby(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.hobby)
                        {
                            var hobi = new ms_hobby();
                            if (item.nama !=null) {
                                hobi = item;
                                hobi.fk_applicant_information_form = pk;
                                db.ms_hobby.Add(hobi);
                            }

                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddSkill(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.keterampilan)
                        {
                            var skill = new ms_skill();
                            if (item.nama !=null) {
                                skill = item;
                                skill.fk_applicant_information_form = pk;
                                db.ms_skill.Add(skill);
                            }

                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddExperience(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.pengalaman_kerja)
                        {

                            var work = new ms_work_experience();
                            if (item.nama_perusahaan != null)
                            {
                                work = item;
                                work.fk_applicant_information_form = pk;
                                db.ms_work_experience.Add(work);
                            }
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddAnotherLanguage(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.bahasa_lain)
                        {
                            var language = new ms_language();
                            if (item.bahasa_asing !=null) {
                                language = item;
                                language.fk_applicant_information_form = pk;
                                db.ms_language.Add(language);
                            }

                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddOrganization(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.organisasi)
                        {

                            var organisasi = new ms_organization();
                            if (item.kegiatan !=null)
                            {
                                organisasi = item;
                                organisasi.fk_applicant_information_form = pk;
                                db.ms_organization.Add(organisasi);
                            }
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddFamily(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.data_keluarga)
                        {
                            var data_keluarga = new ms_family_data();
                            if (item.status_hubungan !=null) {
                                data_keluarga = item;
                                data_keluarga.fk_applicant_information_form = pk;
                                db.ms_family_data.Add(data_keluarga);
                                
                            }
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddInformal(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.pendidikan_informal)
                        {
                            
                            var pendidikan_informal = new ms_informal_education();
                            if (item.jenis !=null) {
                                pendidikan_informal = item;
                                pendidikan_informal.fk_applicant_information_form = pk;
                                db.ms_informal_education.Add(pendidikan_informal);
                            }
                            
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddFormal(Form _post, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        foreach (var item in _post.pendidikan_formal)
                        {
                            var pendidikan_formal = new ms_formal_education();
                            if (item.nama_sekolah !=null) {
                                pendidikan_formal = item;
                                pendidikan_formal.fk_applicant_information_form = pk;
                                db.ms_formal_education.Add(pendidikan_formal);
                            }
                            
                            db.SaveChanges();
                        }
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }

        [HttpPost]
        public ActionResult AddFile(string name_file, HttpPostedFileBase file, int pk)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        string _FileName = Path.GetFileName(file.FileName);
                        string _path = Path.Combine(Server.MapPath("~/File_Employee"), _FileName);
                        //string _FileExt = ;
                        file.SaveAs(_path);
                        var files = new ms_files();
                        files.name_file = name_file;
                        files.location = _FileName;
                        files.fk_applicant_information_form = pk;
                        db.ms_files.Add(files);
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("DetailEmployee", new { id = pk });
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("DetailEmployee", new { id = pk });
        }


    }
}