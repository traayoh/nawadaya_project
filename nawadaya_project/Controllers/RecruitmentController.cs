﻿using nawadaya_project.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace project_nawadaya.Controllers
{
    public class RecruitmentController : Controller
    {

        public ActionResult StartDISC(string obj)
        {
            var query = db.historical_invitation_progress.Where(x=>x.link == obj && x.access_token==1).FirstOrDefault();
            var DISCduration = Convert.ToInt32((db.ms_all_for_one.Where(x => x.pk_all_for_one == 3).FirstOrDefault()).var_value);
            query.endtime = Convert.ToDateTime(DateTime.UtcNow.AddSeconds(DISCduration));
            db.Entry(query).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Disc", new { obj = obj });
        }
        // GET: Recruitment
        public ActionResult Disc(string obj)
        {
            var obj1 = "";
            if (obj != null)
            {
                obj1 = obj.Replace(" ", "+");
            }
            var linkactive = (from s in db.historical_invitation_progress where s.link == obj1 && s.access_token == 1 select s).FirstOrDefault();
            var duration = db.historical_invitation_progress.Where(x => x.link == obj1 && x.access_token==1).FirstOrDefault();
            if (duration.endtime != null)
            {
                ViewBag.TimeDisc = 1;
                ViewBag.EndTime_LT = duration.endtime;
            }
            if (linkactive != null)
            {
                var key = "b14ca5898a4e4133bbce2ea2315a1916";
                var data = DecryptString(key, obj1);
                string[] separate = data.Split('=','&');
                ViewBag.Email = separate[3];
                ViewBag.Link = obj1;
                var email = separate[3];
                var query = (from s in db.ms_personal_data where s.email == email select s).FirstOrDefault();
                var query2 = (from s in db.ms_problem_disc where s.active==1 select s).ToList();
                ViewBag.DISC = query2;
                return View();
            }
            else
            {
                return RedirectToAction("NotFound", "OtherPage");
            }
        }

        public ActionResult CountDISC(string result, string email)
        {

            var pk = (from s in db.ms_personal_data where s.email.Equals(email) select s.pk_employee).FirstOrDefault();
            var link = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == pk && s.access_token == 1 select s.link).FirstOrDefault();
            link = link.Replace(" ", "+");
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var Emailexist = (from s in db.ms_personal_data where s.email.Equals(email) select s).ToList();
                        if (Emailexist.Count > 1)
                        {
                            TempData["message"] = 2;
                            return RedirectToAction("Form", new { obj = link });
                        }
                        else
                        {
                            string[] disc = result.Split(',');
                            
                                var data = new ms_disc_result();
                                data.fk_applicant_information_form = Convert.ToInt16(pk);
                                data.MD = Convert.ToInt16(disc[0]);
                                data.MI = Convert.ToInt16(disc[1]);
                                data.MS = Convert.ToInt16(disc[2]);
                                data.MC = Convert.ToInt16(disc[3]);
                                data.LD = Convert.ToInt16(disc[4]);
                                data.LI = Convert.ToInt16(disc[5]);
                                data.LS = Convert.ToInt16(disc[6]);
                                data.LC = Convert.ToInt16(disc[7]);
                                data.xM = Convert.ToInt16(disc[8]);
                                data.xL = Convert.ToInt16(disc[9]);
                                data.AD = data.MD - data.LD;
                                data.AI = data.MI - data.LI;
                                data.AS = data.MS - data.LS;
                                data.AC = data.MC - data.LC;
                                data.Total_M = data.MD + data.MI + data.MS + data.MC + data.xM;
                                data.Total_L = data.LD + data.LI + data.LS + data.LC + data.xL;
                                data.created_by = email;
                                data.date_created = DateTime.Now;
                                db.ms_disc_result.Add(data);
                                db.SaveChanges();
                                var pk_disc_id = data.pk_disc_result;
                                //tambahan baru disc//start
                                ms_disc_graph_result obj_graph_res = new ms_disc_graph_result();
                                var obj_result = db.ms_disc_result.Where(x => x.pk_disc_result == pk_disc_id).FirstOrDefault();
                                obj_graph_res.fk_disc_result = obj_result.pk_disc_result;

                                //Most
                                var obj_GMD = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.MD).FirstOrDefault();
                                obj_graph_res.RGMD = obj_GMD.GMD;

                                var obj_GMI = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.MI).FirstOrDefault();
                                obj_graph_res.RGMI = obj_GMI.GMI;

                                var obj_GMS = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.MS).FirstOrDefault();
                                obj_graph_res.RGMS = obj_GMS.GMS;

                                var obj_GMC = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.MC).FirstOrDefault();
                                obj_graph_res.RGMC = obj_GMC.GMC;
                                //

                                //Least
                                var obj_GLD = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.LD).FirstOrDefault();
                                obj_graph_res.RGLD = obj_GLD.GLD;

                                var obj_GLI = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.LI).FirstOrDefault();
                                obj_graph_res.RGLI = obj_GLI.GLI;

                                var obj_GLS = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.LS).FirstOrDefault();
                                obj_graph_res.RGLS = obj_GLS.GLS;

                                var obj_GLC = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.LC).FirstOrDefault();
                                obj_graph_res.RGLC = obj_GLC.GLC;
                                //

                                //Change (M-L)=A
                                var obj_GAD = db.ms_graphvalue_change.Where(x => x.tablevalue == (int)obj_result.AD).FirstOrDefault();
                                obj_graph_res.RGAD = obj_GAD.GAD;

                                var obj_GAI = db.ms_graphvalue_change.Where(x => x.tablevalue == (int)obj_result.AI).FirstOrDefault();
                                obj_graph_res.RGAI = obj_GAI.GAI;

                                var obj_GAS = db.ms_graphvalue_change.Where(x => x.tablevalue == (int)obj_result.AS).FirstOrDefault();
                                obj_graph_res.RGAS = obj_GAS.GAS;

                                var obj_GAC = db.ms_graphvalue_change.Where(x => x.tablevalue == (int)obj_result.AC).FirstOrDefault();
                                obj_graph_res.RGAC = obj_GAC.GAC;

                                db.ms_disc_graph_result.Add(obj_graph_res);
                                db.SaveChanges();


                                var pk_graph_to_descr = obj_graph_res.pk_disc_graph_result;
                                //
                                ms_DISC_final_result obj_DISC_final = new ms_DISC_final_result();
                                var obj_graph = db.ms_disc_graph_result.Where(x => x.pk_disc_graph_result == pk_graph_to_descr).FirstOrDefault();
                                obj_DISC_final.fk_disc_graph_result = obj_graph.pk_disc_graph_result;

                                var mostnum = countDISC((double)obj_graph.RGMD, (double)obj_graph.RGMI, (double)obj_graph.RGMS, (double)obj_graph.RGMC);
                                var leastnum = countDISC((double)obj_graph.RGLD, (double)obj_graph.RGLI, (double)obj_graph.RGLS, (double)obj_graph.RGLC);
                                var changenum = countDISC((double)obj_graph.RGAD, (double)obj_graph.RGAI, (double)obj_graph.RGAS, (double)obj_graph.RGAC);

                                var obj_chars_most = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)mostnum).FirstOrDefault();
                                var obj_chars_least = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)leastnum).FirstOrDefault();
                                var obj_chars_change = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)changenum).FirstOrDefault();

                                var obj_descr_change = db.ms_DISC_personal_description.Where(x => x.pk_DISC_personal_description == (long)changenum).FirstOrDefault();
                                var obj_jobsmatch_change = db.ms_DISC_jobs_match.Where(x => x.pk_DISC_jobs_match == (long)changenum).FirstOrDefault();

                                obj_DISC_final.mostA = obj_chars_most.a;
                                obj_DISC_final.mostB = obj_chars_most.b;
                                obj_DISC_final.mostC = obj_chars_most.c;
                                obj_DISC_final.mostD = obj_chars_most.d;
                                obj_DISC_final.mostE = obj_chars_most.e;
                                obj_DISC_final.mostF = obj_chars_most.f;
                                obj_DISC_final.mostG = obj_chars_most.g;
                                obj_DISC_final.mostH = obj_chars_most.h;
                                obj_DISC_final.mostI = obj_chars_most.i;
                                obj_DISC_final.mostJ = obj_chars_most.j;
                                obj_DISC_final.mostK = obj_chars_most.k;
                                obj_DISC_final.mostL = obj_chars_most.l;
                                obj_DISC_final.mostM = obj_chars_most.m;

                                obj_DISC_final.leastA = obj_chars_least.a;
                                obj_DISC_final.leastB = obj_chars_least.b;
                                obj_DISC_final.leastC = obj_chars_least.c;
                                obj_DISC_final.leastD = obj_chars_least.d;
                                obj_DISC_final.leastE = obj_chars_least.e;
                                obj_DISC_final.leastF = obj_chars_least.f;
                                obj_DISC_final.leastG = obj_chars_least.g;
                                obj_DISC_final.leastH = obj_chars_least.h;
                                obj_DISC_final.leastI = obj_chars_least.i;
                                obj_DISC_final.leastJ = obj_chars_least.j;
                                obj_DISC_final.leastK = obj_chars_least.k;
                                obj_DISC_final.leastL = obj_chars_least.l;
                                obj_DISC_final.leastM = obj_chars_least.m;

                                obj_DISC_final.changeA = obj_chars_change.a;
                                obj_DISC_final.changeB = obj_chars_change.b;
                                obj_DISC_final.changeC = obj_chars_change.c;
                                obj_DISC_final.changeD = obj_chars_change.d;
                                obj_DISC_final.changeE = obj_chars_change.e;
                                obj_DISC_final.changeF = obj_chars_change.f;
                                obj_DISC_final.changeG = obj_chars_change.g;
                                obj_DISC_final.changeH = obj_chars_change.h;
                                obj_DISC_final.changeI = obj_chars_change.i;
                                obj_DISC_final.changeJ = obj_chars_change.j;
                                obj_DISC_final.changeK = obj_chars_change.k;
                                obj_DISC_final.changeL = obj_chars_change.l;
                                obj_DISC_final.changeM = obj_chars_change.m;

                                obj_DISC_final.change_descr = obj_descr_change.descr;
                                obj_DISC_final.change_jobs_match = obj_jobsmatch_change.jobs_match;

                                db.ms_DISC_final_result.Add(obj_DISC_final);
                                db.SaveChanges();
                                //tambahan baru disc//end
                           
                            long pk_disc_result = (from s in db.ms_disc_result where s.fk_applicant_information_form == pk orderby s.pk_disc_result descending select s.pk_disc_result).FirstOrDefault();
                            long pk_rekrut = (from s in db.ms_recruit where s.fk_applicant_information_form == pk select s.pk_recruitID).FirstOrDefault();
                            var query = db.ms_recruit.Find(pk_rekrut);
                            if (pk_rekrut != 0)
                            {
                                query.data_crated = DateTime.Now;
                                query.fk_disc_result = pk_disc_result;
                                db.Entry(query).State = EntityState.Modified;
                            }
                            else
                            {
                                var x = new ms_recruit();
                                x.fk_disc_result = pk_disc_result;
                                x.fk_applicant_information_form = pk;
                                x.data_crated = DateTime.Now;
                                db.ms_recruit.Add(x);
                            }
                            var discdone = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == pk && s.access_token == 1 select s).FirstOrDefault();
                            discdone.disc_status = 3;
                            discdone.endtime = null;
                            db.Entry(discdone).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                        }
                    }
                    TempData["message"] = 2;
                    return RedirectToAction("Index", new { obj = link });
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    Console.WriteLine(ex.Message);
                    return RedirectToAction("nulldisc", new { pk = pk, obj = link });
                }
            }

        }

        public ActionResult nulldisc(int pk, string obj)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var result_disc = new ms_disc_result();
                    result_disc.fk_applicant_information_form = pk;
                    db.ms_disc_result.Add(result_disc);
                    db.SaveChanges();
                    var disc_result = (from s in db.ms_disc_result where s.fk_applicant_information_form == pk orderby s.pk_disc_result descending select s).FirstOrDefault();

                    var graph_result = new ms_disc_graph_result();
                    graph_result.fk_disc_result = disc_result.pk_disc_result;
                    db.ms_disc_graph_result.Add(graph_result);
                    db.SaveChanges();

                    var graph_disc_result = (from s in db.ms_disc_graph_result where s.fk_disc_result == disc_result.pk_disc_result orderby s.pk_disc_graph_result descending select s).FirstOrDefault();

                    var final_result = new ms_DISC_final_result();
                    final_result.fk_disc_graph_result = graph_disc_result.pk_disc_graph_result;
                    db.ms_DISC_final_result.Add(final_result);
                    db.SaveChanges();

                    long pk_disc_result = (from s in db.ms_disc_result where s.fk_applicant_information_form == pk orderby s.pk_disc_result descending select s.pk_disc_result).FirstOrDefault();
                    long pk_rekrut = (from s in db.ms_recruit where s.fk_applicant_information_form == pk select s.pk_recruitID).FirstOrDefault();
                    var query = db.ms_recruit.Find(pk_rekrut);
                    if (pk_rekrut != 0)
                    {
                        query.data_crated = DateTime.Now;
                        query.fk_disc_result = pk_disc_result;
                        db.Entry(query).State = EntityState.Modified;
                    }
                    else
                    {
                        var x = new ms_recruit();
                        x.fk_disc_result = pk_disc_result;
                        x.fk_applicant_information_form = pk;
                        x.data_crated = DateTime.Now;
                        db.ms_recruit.Add(x);
                    }


                    var discdone = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == pk && s.access_token == 1 select s).FirstOrDefault();
                    discdone.disc_status = 3;
                    discdone.endtime = null;
                    db.Entry(discdone).State = EntityState.Modified;
                    db.SaveChanges();
                    transaction.Commit();
                    TempData["message"] = 2;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    Console.WriteLine(ex.Message);
                }
            }

            return RedirectToAction("Index", new { obj = obj });
        }

        public static int countDISC(double I2, double J2, double K2, double L2)
        {
            int countDEF = 1;
            if (I2 <= 0 && J2 <= 0 && K2 <= 0 && L2 > 0)//1//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 <= 0 && K2 <= 0 && L2 <= 0)//2//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 <= 0 && K2 <= 0 && L2 > 0 && L2 >= I2)//3//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && K2 <= 0 && L2 <= 0 && J2 >= I2)//4//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && K2 <= 0 && L2 > 0 && J2 >= I2 && I2 >= L2)//5//(I2>0&&J2>0&&K2<=0&&L2>0&&J2>=I2>=L2)

            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && J2 >= I2 && I2 >= K2)//6//(I2>0&&J2>0&&K2>0&&L2<=0&&J2>=I2>=K2)
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && J2 >= K2 && K2 >= I2)//7//(I2>0&&J2>0&&K2>0&&L2<=0&&J2>=K2>=I2)
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 <= 0 && K2 > 0 && L2 > 0 && K2 >= I2 && I2 >= L2)//8//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && K2 <= 0 && L2 <= 0 && I2 >= J2)//9//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && I2 >= J2 && J2 >= K2)//10//(I2>0&&J2>0&&K2>0&&L2<=0&&I2>=J2>=K2)
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 <= 0 && K2 > 0 && L2 <= 0 && I2 >= K2)//11//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 <= 0 && J2 > 0 && K2 > 0 && L2 > 0 && L2 >= J2 && J2 >= K2)//12//(I2<=0&&J2>0&&K2>0&&L2>0&&L2>=J2>=K2)
            {
                return countDEF;
            }
            countDEF++;
            if (I2 <= 0 && J2 > 0 && K2 > 0 && L2 > 0 && L2 >= K2 && K2 >= J2)//13//(I2<=0&&J2>0&&K2>0&&L2>0&&L2>=K2>=J2)
            {
                return countDEF;
            }
            countDEF++;
            if (I2 <= 0 && J2 > 0 && K2 > 0 && L2 > 0 && J2 >= K2 && J2 >= L2)//14//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 <= 0 && J2 <= 0 && K2 > 0 && L2 <= 0)//15//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 <= 0 && J2 <= 0 && K2 > 0 && L2 > 0 && L2 >= K2)//16//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 <= 0 && J2 <= 0 && K2 > 0 && L2 > 0 && K2 >= L2)//17//
            {
                return countDEF;
            }
            countDEF++;
            if (J2 <= 0 && K2 <= 0 && I2 > 0 && L2 > 0 && I2 >= L2)//18//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && I2 >= J2 && J2 >= L2)//19//(I2>0&&J2>0&&L2>0&&K2<=0&&I2>=J2>=L2)
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && K2 > 0 && J2 > 0 && L2 <= 0 && I2 >= K2 && K2 >= J2)//20//(I2>0&&K2>0&&J2>0&&L2<=0&&I2>=K2>=J2)
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && K2 > 0 && L2 > 0 && J2 <= 0 && I2 >= K2 && K2 >= L2)//21//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && I2 >= L2 && L2 >= J2)//22//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && K2 > 0 && L2 > 0 && J2 <= 0 && I2 >= L2 && L2 >= K2)//23//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 <= 0 && K2 <= 0 && L2 <= 0 && J2 > 0)//24//
            {
                return countDEF;
            }
            countDEF++;
            if (J2 > 0 && K2 > 0 && I2 <= 0 && L2 <= 0 && J2 >= K2)//25//
            {
                return countDEF;
            }
            countDEF++;
            if (J2 > 0 && L2 > 0 && I2 <= 0 && K2 <= 0 && J2 >= L2)//26//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && J2 >= L2 && L2 >= I2)//27//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 <= 0 && J2 > 0 && K2 > 0 && L2 > 0 && J2 >= L2 && L2 >= K2)//28//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 <= 0 && K2 > 0 && L2 <= 0 && K2 >= I2)//29//
            {
                return countDEF;
            }
            countDEF++;
            if (J2 > 0 && K2 > 0 && I2 <= 0 && L2 <= 0 && K2 >= J2)//30//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && K2 >= I2 && I2 >= J2)//31//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && K2 >= J2 && J2 >= I2)//32//
            {
                return countDEF;
            }
            countDEF++;
            if (J2 > 0 && K2 > 0 && L2 > 0 && I2 <= 0 && K2 >= J2 && J2 >= L2)//33//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 <= 0 && K2 > 0 && L2 > 0 && K2 >= L2 && L2 >= I2)//34//
            {
                return countDEF;
            }
            countDEF++;
            if (J2 > 0 && K2 > 0 && L2 > 0 && I2 <= 0 && K2 >= L2 && L2 >= J2)//35//
            {
                return countDEF;
            }
            countDEF++;
            if (J2 > 0 && L2 > 0 && I2 <= 0 && K2 <= 0 && L2 >= J2)//36//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && L2 >= I2 && I2 >= J2)//37//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && K2 > 0 && L2 > 0 && J2 <= 0 && L2 >= I2 && I2 >= K2)//38//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && L2 >= J2 && J2 >= I2)//39//
            {
                return countDEF;
            }
            countDEF++;
            if (I2 > 0 && K2 > 0 && L2 > 0 && J2 <= 0 && L2 >= K2 && K2 >= I2)//40//
            {
                return countDEF;
            }
            countDEF++;

            return countDEF;
        }

        public ActionResult Form(string obj)
        {
            var obj1 = "";
            if (obj != null)
            {
                obj1 = obj.Replace(" ", "+");
            }
            var linkactive = (from s in db.historical_invitation_progress where s.link == obj1 && s.access_token == 1 select s).FirstOrDefault();
            Form dbx = new Form();
            if (linkactive != null)
            {
                var key = "b14ca5898a4e4133bbce2ea2315a1916";
                var data = DecryptString(key, obj1);
                string[] separate = data.Split('=','&');
                ViewBag.Email = separate[3];
                ViewBag.Link = obj1;
                var family = new List<Family>();
                family.Add(new Family() { Value = "Ayah", Text = "Ayah" });
                family.Add(new Family() { Value = "Ibu", Text = "Ibu" });
                family.Add(new Family() { Value = "Kakak", Text = "Kakak" });
                family.Add(new Family() { Value = "Adik", Text = "Adik" });
                ViewBag.Family = family;
                var religion = new List<Religion>();
                religion.Add(new Religion() { Value = "Islam", Text = "Islam" });
                religion.Add(new Religion() { Value = "Kristen", Text = "Kristen" });
                religion.Add(new Religion() { Value = "Hindu", Text = "Hindu" });
                religion.Add(new Religion() { Value = "Budha", Text = "Budha" });
                ViewBag.Religion = religion;
                var country = new List<Country>();
                country.Add(new Country() { Value = "Bandung", Text = "Bandung" });
                country.Add(new Country() { Value = "Jakarta", Text = "Jakarta" });
                country.Add(new Country() { Value = "Depok", Text = "Depok" });
                country.Add(new Country() { Value = "Subang", Text = "Subang" });
                ViewBag.Country = country;
                var result = DateTime.Now.Year;
                var year = new List<SelectType>();
                for (int i = Convert.ToInt16(result); i > 1990; i--) {
                    year.Add(new SelectType() { Value = i.ToString(), Text = i.ToString() });
                }
                ViewBag.Year = year;
                var education = new List<Education>();
                education.Add(new Education() { Value = "SD", Text = "SD" });
                education.Add(new Education() { Value = "SMP", Text = "SMP" });
                education.Add(new Education() { Value = "SMA", Text = "SMA" });
                education.Add(new Education() { Value = "S1", Text = "S1" });
                ViewBag.Education = education;

                //Form a = new Form();
                //a.pertanyaan.alasan_masuk_nawadata = "Adanya Jenjang karir yang jelas pada perusahaan ini";
                //a.pertanyaan.hal_terpenting = "Orang Tua";
                //a.pertanyaan.ambisi = "Menjadi orang yang berguna di lingkungan sekitar";
                //a.pertanyaan.pengertian_sukses = "Ketika menjadi berguna untuk nusa dan bangsa";
                //a.pertanyaan.etika_moral = "Tingkah laku";
                //a.pertanyaan.pendapat_orang_tentang_anda = "Ramah dan humble";
                //a.pertanyaan.kekurangan = "Menjadi orang yang berguna di lingkungan sekitar";
                //a.pertanyaan.kelebihan = "Ketika menjadi berguna untuk nusa dan bangsa";
                //a.pertanyaan.pertanyaan_masalah = "Ketika menjadi berguna untuk nusa dan bangsa";
                //a.pertanyaan.keinginan_jabatan = "CEO";



                return View();
            }
            else
            {
                return RedirectToAction("NotFound", "OtherPage");
            }

        }

        [HttpPost]
        public ActionResult EditApplicant(Form _post, string link, HttpPostedFileBase cv, HttpPostedFileBase photo, string cv_title,string photo_title)
        {

            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    link = link.Replace(" ", "+");
                    var Emailexist = (from s in db.ms_personal_data where s.email.Equals(_post.data_pribadi.email) select s).ToList();
                    if (Emailexist.Count > 1)
                    {
                        TempData["message"] = 2;
                        return RedirectToAction("Form", new { obj = link });
                    }
                    else
                    {
                        var pk = (from s in db.ms_personal_data where s.email.Equals(_post.data_pribadi.email) select s.pk_employee).FirstOrDefault();
                        var data = (from s in db.ms_personal_data where s.pk_employee == pk select s).FirstOrDefault();
                        if (data != null)
                        {
                            data.full_name = _post.data_pribadi.full_name;
                            data.name_called = _post.data_pribadi.name_called;
                            data.address = _post.data_pribadi.address;
                            data.birth_place = _post.data_pribadi.birth_place;
                            data.birth_date = _post.data_pribadi.birth_date;
                            data.email = _post.data_pribadi.email;
                            data.gaji = _post.data_pribadi.gaji;
                            data.gender = _post.data_pribadi.gender;
                            data.home_phone = _post.data_pribadi.home_phone;
                            data.kemampuan = _post.data_pribadi.kemampuan;
                            data.marriage_status = _post.data_pribadi.marriage_status;
                            data.NIK = _post.data_pribadi.NIK;
                            data.NIP = _post.data_pribadi.NIP;
                            data.phone = _post.data_pribadi.phone;
                            data.religion = _post.data_pribadi.religion;
                            data.status_employee = 1;
                            data.created_by = _post.data_pribadi.full_name;
                            data.date_created = DateTime.Now;
                            db.Entry(data).State = EntityState.Modified;
                            db.SaveChanges();
                            long pk_rekrut = (from s in db.ms_recruit where s.fk_applicant_information_form == pk select s.pk_recruitID).FirstOrDefault();
                            var query = db.ms_recruit.Find(pk_rekrut);
                            if (pk_rekrut != 0)
                            {
                                query.created_by = _post.data_pribadi.full_name;
                                query.data_crated = DateTime.Now;
                                query.fk_applicant_information_form = pk;
                                db.Entry(query).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                var a = new ms_recruit();
                                a.fk_applicant_information_form = pk;
                                a.created_by = _post.data_pribadi.full_name;
                                a.data_crated = DateTime.Now;
                                db.ms_recruit.Add(a);
                                db.SaveChanges();
                            }
                            long pk_pertanyaan = (from s in db.ms_question where s.fk_employee == pk select s.pk_pertanyaan).FirstOrDefault();
                            var query1 = db.ms_question.Find(pk_pertanyaan);
                            if (pk_pertanyaan != 0)
                            {
                                query1.alasan_masuk_nawadata = _post.pertanyaan.alasan_masuk_nawadata;
                                query1.ambisi = _post.pertanyaan.ambisi;
                                query1.etika_moral = _post.pertanyaan.etika_moral;
                                query1.fk_employee = pk;
                                query1.hal_terpenting = _post.pertanyaan.hal_terpenting;
                                query1.keinginan_jabatan = _post.pertanyaan.keinginan_jabatan;
                                query1.kekurangan = _post.pertanyaan.kekurangan;
                                query1.kelebihan = _post.pertanyaan.kelebihan;
                                query1.keterangan_kecelakaan = _post.pertanyaan.keterangan_kecelakaan;
                                query1.keterangan_masalah = _post.pertanyaan.keterangan_masalah;
                                query1.keterangan_psikotes = _post.pertanyaan.keterangan_psikotes;
                                query1.keterangan_sakit = _post.pertanyaan.keterangan_sakit;
                                query1.pendapat_orang_tentang_anda = _post.pertanyaan.pendapat_orang_tentang_anda;
                                query1.pengertian_sukses = _post.pertanyaan.pengertian_sukses;
                                query1.pertanyaan_kecelakaan = _post.pertanyaan.pertanyaan_kecelakaan;
                                query1.pertanyaan_masalah = _post.pertanyaan.pertanyaan_masalah;
                                query1.pertanyaan_psikotes = _post.pertanyaan.pertanyaan_psikotes;
                                query1.pertanyaan_sakit = _post.pertanyaan.pertanyaan_sakit;
                                query1.waktu_bergabung = _post.pertanyaan.waktu_bergabung;
                                db.Entry(query1).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                var b = new ms_question();
                                b = _post.pertanyaan;
                                b.fk_employee = pk;
                                db.ms_question.Add(b);
                                db.SaveChanges();
                            }

                            var bahasa_lain = (from s in db.ms_language where s.fk_applicant_information_form == pk select s).ToList();
                            if (bahasa_lain != null)
                            {
                                foreach (var item in bahasa_lain)
                                {
                                    db.ms_language.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.bahasa_lain != null)
                            {
                                foreach (var item in _post.bahasa_lain)
                                {
                                    if (item.bahasa_asing != null)
                                    {
                                        var c = new ms_language();
                                        c = item;
                                        c.created_by = _post.data_pribadi.full_name;
                                        c.date_created = DateTime.Now;
                                        c.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_language.Add(c);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var data_keluarga = (from s in db.ms_family_data where s.fk_applicant_information_form == pk select s).ToList();
                            if (data_keluarga != null)
                            {
                                foreach (var item in data_keluarga)
                                {
                                    db.ms_family_data.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.data_keluarga != null)
                            {
                                foreach (var item in _post.data_keluarga)
                                {
                                    if (item.nama != null)
                                    {
                                        var d = new ms_family_data();
                                        d = item;
                                        d.created_by = _post.data_pribadi.full_name;
                                        d.date_created = DateTime.Now;
                                        d.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_family_data.Add(d);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var hobi = (from s in db.ms_hobby where s.fk_applicant_information_form == pk select s).ToList();
                            if (hobi != null)
                            {
                                foreach (var item in hobi)
                                {
                                    db.ms_hobby.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.hobby != null)
                            {
                                foreach (var item in _post.hobby)
                                {
                                    if (item.nama != null)
                                    {
                                        var e = new ms_hobby();
                                        e = item;
                                        e.created_by = _post.data_pribadi.full_name;
                                        e.date_created = DateTime.Now;
                                        e.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_hobby.Add(e);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var keluarga_darurat = (from s in db.ms_emergency_family where s.fk_applicant_information_form == pk select s).ToList();
                            if (keluarga_darurat != null)
                            {
                                foreach (var item in keluarga_darurat)
                                {
                                    db.ms_emergency_family.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.keluarga_darurat != null)
                            {
                                foreach (var item in _post.keluarga_darurat)
                                {
                                    if (item.nama != null)
                                    {
                                        var f = new ms_emergency_family();
                                        f = item;
                                        f.created_by = _post.data_pribadi.full_name;
                                        f.date_created = DateTime.Now;
                                        f.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_emergency_family.Add(f);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var keterampilan = (from s in db.ms_skill where s.fk_applicant_information_form == pk select s).ToList();
                            if (keterampilan != null)
                            {
                                foreach (var item in keterampilan)
                                {
                                    db.ms_skill.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.keterampilan != null)
                            {
                                foreach (var item in _post.keterampilan)
                                {
                                    if (item.nama != null)
                                    {
                                        var g = new ms_skill();
                                        g = item;
                                        g.created_by = _post.data_pribadi.full_name;
                                        g.date_created = DateTime.Now;
                                        g.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_skill.Add(g);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var organisasi = (from s in db.ms_organization where s.fk_applicant_information_form == pk select s).ToList();
                            if (organisasi != null)
                            {
                                foreach (var item in organisasi)
                                {
                                    db.ms_organization.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.organisasi != null)
                            {
                                foreach (var item in _post.organisasi)
                                {
                                    if (item.kegiatan != null)
                                    {
                                        var h = new ms_organization();
                                        h = item;
                                        h.created_by = _post.data_pribadi.full_name;
                                        h.date_created = DateTime.Now;
                                        h.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_organization.Add(h);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var pekerjaan_fav = (from s in db.ms_favorite_job where s.fk_applicant_information_form == pk select s).ToList();
                            if (pekerjaan_fav != null)
                            {
                                foreach (var item in pekerjaan_fav)
                                {
                                    db.ms_favorite_job.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.pekerjaan_favorite != null)
                            {
                                foreach (var item in _post.pekerjaan_favorite)
                                {
                                    if (item.nama != null)
                                    {
                                        var i = new ms_favorite_job();
                                        i = item;
                                        i.created_by = _post.data_pribadi.full_name;
                                        i.date_created = DateTime.Now;
                                        i.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_favorite_job.Add(i);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var pendidikan_formal = (from s in db.ms_formal_education where s.fk_applicant_information_form == pk select s).ToList();
                            if (pendidikan_formal != null)
                            {
                                foreach (var item in pendidikan_formal)
                                {
                                    db.ms_formal_education.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.pendidikan_formal != null)
                            {
                                foreach (var item in _post.pendidikan_formal)
                                {
                                    if (item.nama_sekolah != null)
                                    {
                                        var j = new ms_formal_education();
                                        j = item;
                                        j.keterangan = item.keterangan;
                                        j.score = Convert.ToInt16(item.score);
                                        j.created_by = _post.data_pribadi.full_name;
                                        j.date_created = DateTime.Now;
                                        j.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_formal_education.Add(j);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var pendidikan_informal = (from s in db.ms_informal_education where s.fk_applicant_information_form == pk select s).ToList();
                            if (pendidikan_informal != null)
                            {
                                foreach (var item in pendidikan_informal)
                                {
                                    db.ms_informal_education.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.pendidikan_informal != null)
                            {
                                foreach (var item in _post.pendidikan_informal)
                                {
                                    if (item.jenis != null)
                                    {
                                        var k = new ms_informal_education();
                                        k = item;
                                        k.created_by = _post.data_pribadi.full_name;
                                        k.date_created = DateTime.Now;
                                        k.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_informal_education.Add(k);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var pengalaman_kerja = (from s in db.ms_work_experience where s.fk_applicant_information_form == pk select s).ToList();
                            if (pengalaman_kerja != null)
                            {
                                foreach (var item in pengalaman_kerja)
                                {
                                    db.ms_work_experience.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.pengalaman_kerja != null)
                            {
                                foreach (var item in _post.pengalaman_kerja)
                                {
                                    if (item.nama_perusahaan != null)
                                    {
                                        var l = new ms_work_experience();
                                        l = item;
                                        l.created_by = _post.data_pribadi.full_name;
                                        l.date_created = DateTime.Now;
                                        l.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_work_experience.Add(l);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var referensi = (from s in db.ms_reference where s.fk_applicant_information_form == pk select s).ToList();
                            if (referensi != null)
                            {
                                foreach (var item in referensi)
                                {
                                    db.ms_reference.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            if (_post.referensi != null)
                            {
                                foreach (var item in _post.referensi)
                                {
                                    if (item.nama_perusahaan != null)
                                    {
                                        var m = new ms_reference();
                                        m = item;
                                        m.created_by = _post.data_pribadi.full_name;
                                        m.date_created = DateTime.Now;
                                        m.fk_applicant_information_form = Convert.ToInt16(pk);
                                        db.ms_reference.Add(m);
                                    }
                                }
                                db.SaveChanges();
                            }

                            var file = (from s in db.ms_files where s.fk_applicant_information_form == pk select s).ToList();
                            if (file != null)
                            {
                                foreach (var item in file)
                                {
                                    db.ms_files.Remove(item);
                                }
                                db.SaveChanges();
                            }
                            string _FileCv = Path.GetFileName(cv.FileName);
                            string _pathcv = Path.Combine(Server.MapPath("~/File_Employee"), _FileCv);
                            //string _FileExt = ;
                            cv.SaveAs(_pathcv);

                            var cv_file = new ms_files();
                            cv_file.name_file = cv_title;
                            cv_file.location = _FileCv;
                            cv_file.fk_applicant_information_form = Convert.ToInt16(pk);
                            db.ms_files.Add(cv_file);

                            string _FilePhoto = Path.GetFileName(photo.FileName);
                            string _pathphoto = Path.Combine(Server.MapPath("~/File_Employee"), _FilePhoto);
                            //string _FileExt = ;
                            photo.SaveAs(_pathphoto);

                            var cv_photo = new ms_files();
                            cv_photo.name_file = photo_title;
                            cv_photo.location = _FilePhoto;
                            cv_photo.fk_applicant_information_form = Convert.ToInt16(pk);
                            db.ms_files.Add(cv_photo);
                            
                            var formdone = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == pk && s.access_token == 1 select s).FirstOrDefault();
                            formdone.applicant_information_status = 3;
                            db.Entry(formdone).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 1;
                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }

                return RedirectToAction("Index", new { obj = link });
            }

            
        }

        [HttpPost]
        public ActionResult UploadLogic(string file, string link,string email)
        {
            link = link.Replace(" ", "+");
            if (ModelState.IsValid)
            {

                    var query = new ms_logictest_result();
                    query.logictest_solutions = "logic.zip";
                    query.date_created = DateTime.Now;
                    query.date_test = DateTime.Now;
                    query.created_by = email;
                    db.ms_logictest_result.Add(query);
                    db.SaveChanges();
                    int fk_applicant = Convert.ToInt16((from s in db.ms_personal_data where s.email == email select s.pk_employee).FirstOrDefault());
                    long pk_rekrut = (from s in db.ms_recruit where s.fk_applicant_information_form == fk_applicant select s.pk_recruitID).FirstOrDefault();
                    var query4 = db.ms_recruit.Find(pk_rekrut);
                    if (pk_rekrut != 0)
                    {
                        query.date_created = DateTime.Now;
                        query.fk_applicant_information_form = fk_applicant;
                        db.Entry(query).State = EntityState.Modified;
                    }
                    else
                    {
                        var x = new ms_recruit();
                        x.fk_applicant_information_form = fk_applicant;
                        x.data_crated = DateTime.Now;
                        db.ms_recruit.Add(x);
                    }
                    var logicdone = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == fk_applicant && s.access_token == 1 select s).FirstOrDefault();
                    logicdone.logictest_status = 3;
                    db.Entry(logicdone).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index", new { obj = link });
            }
            ViewBag.message = 0;
            return RedirectToAction("Index", new { obj = link });
        }

        public ActionResult Durationpsikotes(string obj,string subtest,string answer)
        {
            var obj1 = "";
            if (obj != null)
            {
                obj1 = obj.Replace(" ", "+");
            }
            if (subtest == "1" || subtest == "3" || subtest == "5" || subtest == "7")
            {
                if (subtest == "1") {
                    var query = db.historical_invitation_progress.Where(x => x.link == obj1).FirstOrDefault();
                    query.endtime = Convert.ToDateTime(DateTime.UtcNow.AddSeconds(180));
                    query.created_by = subtest;
                    db.Entry(query).State = EntityState.Modified;
                    db.SaveChanges();
                } else if (subtest == "3") {
                    var query = db.historical_invitation_progress.Where(x => x.link == obj1).FirstOrDefault();
                    query.endtime = Convert.ToDateTime(DateTime.UtcNow.AddSeconds(240));
                    query.created_by = subtest;
                    db.Entry(query).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else if (subtest == "5")
                {
                    var query = db.historical_invitation_progress.Where(x => x.link == obj1).FirstOrDefault();
                    query.endtime = Convert.ToDateTime(DateTime.UtcNow.AddSeconds(180));
                    query.created_by = subtest;
                    db.Entry(query).State = EntityState.Modified;
                    db.SaveChanges();
                }
                else if (subtest == "7")
                {
                    var query = db.historical_invitation_progress.Where(x => x.link == obj1).FirstOrDefault();
                    query.endtime = Convert.ToDateTime(DateTime.UtcNow.AddSeconds(150));
                    query.created_by = subtest;
                    db.Entry(query).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            var query2 = db.historical_invitation_progress.Where(x => x.link == obj1).FirstOrDefault();
            query2.created_by = subtest;
            query2.answer_psikotes = answer;
            db.Entry(query2).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Psikotes", new { obj = obj });
        }

        public ActionResult Psikotes(string obj)
        {
            var obj1 = "";
            if (obj != null)
            {
                obj1 = obj.Replace(" ", "+");
            }
            var linkactive = (from s in db.historical_invitation_progress where s.link == obj1 && s.access_token == 1 select s).FirstOrDefault();
            var duration = db.historical_invitation_progress.Where(x => x.link == obj1).FirstOrDefault();
            if (duration.created_by !=null) {
                ViewBag.Subtest = duration.created_by.Substring(0, 1);
                ViewBag.Answerpsikotest = duration.answer_psikotes;
            }
            if (ViewBag.Subtest == "1" || ViewBag.Subtest == "3" || ViewBag.Subtest == "5" || ViewBag.Subtest == "7")
            {
                //if (ViewBag.Subtest != "1")
                //{
                //    ViewBag.Answerpsikotest = duration.created_by.Substring(2, duration.created_by.Length - 2);

                //}
                
                ViewBag.duration = duration.endtime;
            }
            if (linkactive != null)
            {
                var key = "b14ca5898a4e4133bbce2ea2315a1916";
                var data = DecryptString(key, obj1);
                string[] separate = data.Split('=','&');
                var email = separate[3];
                ViewBag.Email = email;
                ViewBag.Link = obj1;
                ViewBag.Psikotes1 = (from s in db.ms_psychotest_problems where s.step == "Step 1" && s.active == 1 select s).ToList();
                var Answer1 = (from s in db.ms_psychotest_problems where s.step == "Example Step 1" && s.active == 1 select s).ToList();
                ViewBag.Example1 = Answer1;
                string answer1 = "";
                foreach (var item in Answer1) {
                    answer1 += item.answer;
                }
                ViewBag.answer = answer1;

                ViewBag.Psikotes2 = (from s in db.ms_psychotest_problems where s.step == "Step 2" && s.active == 1 select s).ToList();
                var Answer2 = (from s in db.ms_psychotest_problems where s.step == "Example Step 2" && s.active == 1 select s).ToList();
                ViewBag.Example2 = Answer2;
                string answer2 = "";
                foreach (var item in Answer2)
                {
                    answer2 += item.answer;
                }
                ViewBag.answer2 = answer2;

                ViewBag.Psikotes3 = (from s in db.ms_psychotest_problems where s.step == "Step 3" && s.active == 1 select s).ToList();
                var Answer3 = (from s in db.ms_psychotest_problems where s.step == "Example Step 3" && s.active == 1 select s).ToList();
                ViewBag.Example3 = Answer3;
                string answer3 = "";
                foreach (var item in Answer3)
                {
                    answer3 += item.answer;
                }
                ViewBag.answer3 = answer3;

                ViewBag.Psikotes4 = (from s in db.ms_psychotest_problems where s.step == "Step 4" && s.active == 1 select s).ToList();
                var Answer4 = (from s in db.ms_psychotest_problems where s.step == "Example Step 4" && s.active == 1 select s).ToList();
                ViewBag.Example4 = Answer4;
                string answer4 = "";
                foreach (var item in Answer4)
                {
                    answer4 += item.answer;
                }
                ViewBag.answer4 = answer4;

            }
            else
            {
                return RedirectToAction("NotFound", "OtherPage");
            }

            return View();
        }

        public ActionResult CountPsikotes(string result, string email)
        {
            var pk = (from s in db.ms_personal_data where s.email.Equals(email) select s.pk_employee).FirstOrDefault();
            var link = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == pk && s.access_token == 1 select s.link).FirstOrDefault();
            link = link.Replace(" ", "+");
            var query2 = db.historical_invitation_progress.Where(x => x.link == link).FirstOrDefault();
            query2.created_by = null;
            db.Entry(query2).State = EntityState.Modified;
            db.SaveChanges();
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var Emailexist = (from s in db.ms_personal_data where s.email.Equals(email) select s).ToList();
                        if (Emailexist.Count > 1)
                        {
                            TempData["message"] = 2;
                            return RedirectToAction("Form", new { obj = link });
                        }
                        else
                        {
                            string[] psychotest = result.Split('-');
                            string[] sub1 = psychotest[0].Split(',');
                            string[] sub2 = psychotest[1].Split(new[] { "p" }, StringSplitOptions.None);
                            string[] sub3 = psychotest[2].Split(',');
                            string[] sub4 = psychotest[3].Split(',');
                            sub2 = sub2.Where(val => val != "").ToArray();

                            int a = 0, score = 0;
                            var subtest1 = (from s in db.ms_psychotest_problems where s.step == "Step 1" select s).ToList();
                            foreach (var item in subtest1)
                            {
                                if (sub1[a] == item.answer)
                                {
                                    score += 1;
                                }
                                a++;
                            }
                            a = 0;
                            var subtest2 = (from s in db.ms_psychotest_problems where s.step == "Step 2" select s).ToList();
                            foreach (var item in subtest2)
                            {
                                char c = sub2[a].Last();
                                if (c == ',')
                                {
                                    sub2[a] = sub2[a].Substring(0, sub2[a].Length - 1);
                                }
                                if (sub2[a] != "null")
                                {
                                    sub2[a] = sub2[a].Substring(5, sub2[a].Length - 5);
                                }
                                if (sub2[a] == item.answer)
                                {
                                    score += 1;
                                }
                                a++;
                            }
                            a = 0;
                            var subtest3 = (from s in db.ms_psychotest_problems where s.step == "Step 3" select s).ToList();
                            foreach (var item in subtest3)
                            {
                                if (sub3[a] == item.answer)
                                {
                                    score += 1;
                                }
                                a++;
                            }
                            a = 0;
                            var subtest4 = (from s in db.ms_psychotest_problems where s.step == "Step 4" select s).ToList();
                            foreach (var item in subtest4)
                            {
                                if (sub4[a] == item.answer)
                                {
                                    score += 1;
                                }
                                a++;
                            }

                                var data = new ms_psychotest_result();
                                data.fk_applicant = pk;
                                data.score = score;
                                data.fk_CFIT_to_IQ = (db.ms_CFIT_to_IQ.Where(x => x.CFIT_rawscore == score).FirstOrDefault()).pk_CFIT_to_IQ;
                                data.create_by = email;
                                data.date_crated = DateTime.Now;
                                db.ms_psychotest_result.Add(data);
                                db.SaveChanges();

                            long pk_psychotest_result = (from s in db.ms_psychotest_result where s.fk_applicant == pk orderby s.pk_psychotest_result descending select s.pk_psychotest_result).FirstOrDefault();
                            long pk_rekrut = (from s in db.ms_recruit where s.fk_applicant_information_form == pk select s.pk_recruitID).FirstOrDefault();
                            var query = db.ms_recruit.Where(x=>x.fk_applicant_information_form == pk).FirstOrDefault();
                            if (query != null)
                            {
                                query.data_crated = DateTime.Now;
                                query.fk_psychotest_result = pk_psychotest_result;
                                db.Entry(query).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                var x = new ms_recruit();
                                x.fk_psychotest_result = pk_psychotest_result;
                                x.fk_applicant_information_form = pk;
                                x.data_crated = DateTime.Now;
                                db.ms_recruit.Add(x);
                            }
                            var psychotestdone = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == pk && s.access_token == 1 select s).FirstOrDefault();
                            psychotestdone.psychotest_status = 3;
                            psychotestdone.endtime = null;
                            psychotestdone.answer_psikotes = null;
                            db.Entry(psychotestdone).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                        }
                    }
                    TempData["message"] = 4;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }


            return RedirectToAction("Index", new { obj = link });
        }

        public ActionResult TestLogic(string obj)
        {
            var obj1 = obj.Replace(" ", "+");
            var linkactive = (from s in db.historical_invitation_progress where s.link == obj1 && s.access_token == 1 select s).FirstOrDefault();
            if (linkactive != null)
            {
                var key = "b14ca5898a4e4133bbce2ea2315a1916";
                var data = DecryptString(key, obj1);
                string[] separate = data.Split('=','&');
                ViewBag.Email = separate[3];
                ViewBag.Link = obj1;

                //Start/Tambahan dari Kms/
                string str_email = separate[3];
                var obj_logictest = db.ms_personal_data.Where(x => x.email == str_email).FirstOrDefault();
                ViewBag.ID = obj_logictest.pk_employee;
                //End/Tambahan dari Kms/

                return View();
            }
            else
            {
                return RedirectToAction("NotFound", "OtherPage");
            }
        }

        public ActionResult nulltestlogic(string obj)
        {

            var obj1 = obj.Replace(" ", "+");
 
                var key = "b14ca5898a4e4133bbce2ea2315a1916";
                var data = DecryptString(key, obj1);
                string[] separate = data.Split('=', '&');
                
                //Start/Tambahan dari Kms/
                string str_email = separate[3];
            var obj_logictest = db.ms_personal_data.Where(x => x.email == str_email).FirstOrDefault();
            List<ms_logictest_result> list_latest_examinee = db.ms_logictest_result.Where(x => x.fk_applicant_information_form == obj_logictest.pk_employee).ToList();
            var obj_latest_examinee_pk = list_latest_examinee.OrderBy(item => item.pk_logictest_result).Last();

            if (obj_latest_examinee_pk.logictest_solutions == null)
            {
                var obj_zip = db.ms_logictest_result.Where(x => x.pk_logictest_result == (int)obj_latest_examinee_pk.pk_logictest_result).FirstOrDefault();
                obj_zip.logictest_solutions = null;
                //obj_zip.fk_applicant_information_form = fk_applicant;
                obj_zip.logictest_solutions_path = null;
                obj_zip.date_created = DateTime.Now;
                obj_zip.date_test = DateTime.Now;
                obj_zip.logictest_score = 0;
                obj_zip.note = "applicant didn't upload solution";
                obj_zip.created_by = str_email;
                //var obtest = db.historical_invitation_progress.Where(x => x.pk_historical_invitation_progress == (int)obj_zip.fk_applicant_information_form).FirstOrDefault
                db.Entry(obj_zip).State = EntityState.Modified;
                db.SaveChanges();
                long pk_rekrut = (from s in db.ms_recruit where s.fk_applicant_information_form == obj_logictest.pk_employee select s.pk_recruitID).FirstOrDefault();
                var query4 = db.ms_recruit.Find(pk_rekrut);
                if (pk_rekrut != 0)
                {
                    query4.data_crated = DateTime.Now;
                    query4.fk_logictest_result = obj_zip.pk_logictest_result;
                    db.Entry(query4).State = EntityState.Modified;
                }
                else
                {
                    var x = new ms_recruit();
                    x.fk_logictest_result = obj_zip.pk_logictest_result;
                    x.data_crated = DateTime.Now;
                    db.ms_recruit.Add(x);
                }
            }
            var logictestdone = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == obj_logictest.pk_employee && s.access_token == 1 select s).FirstOrDefault();
            logictestdone.logictest_status = 3;
            db.Entry(logictestdone).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", new { obj = obj1 });
        }

        private Nawadaya_Entities db = new Nawadaya_Entities();

        [HttpGet]
        public ActionResult Index(string obj)
        {
            var obj1 = "";
            if (obj != null) {
                obj1 = obj.Replace(" ", "+");
            }
            var fk_applicant = (from s in db.historical_invitation_progress where s.link == obj1 && s.access_token == 1 select s.fk_applicant_information_form).FirstOrDefault();
            var linkactive = (from s in db.historical_invitation_progress where s.link == obj1 && s.access_token == 1 select s).FirstOrDefault();
            if (linkactive != null) {
                 if (linkactive.starttime <= DateTime.Now && linkactive.expiredtime >= DateTime.Now)
                {
                    var key = "b14ca5898a4e4133bbce2ea2315a1916";
                    var data = DecryptString(key, obj1);
                    string[] separate = data.Split('=', '&');
                    var problem = separate[1];
                    var email = separate[3];
                    List<string> item = new List<string>();
                    string[] arrayproblem = problem.Split(',');
                    int a = 1;
                    for (int i = 0; i < arrayproblem.Length - 1; i++)
                    {
                        int link = Convert.ToInt32(arrayproblem[i]);
                        if (a != 0)
                        {
                            var b = (from s in db.ms_problem where s.pk_problem.Equals(1) select s.link_problem).First();
                            item.Add(b);
                            a = 0;
                        }
                        var query = (from s in db.ms_problem where s.pk_problem.Equals(link) select s.link_problem).First();
                        item.Add(query);
                    }
                    var personal = db.ms_personal_data.Where(x=>x.email == email).FirstOrDefault();
                    ViewBag.Fullname = personal.full_name;
                    ViewBag.Problem = item;
                    ViewBag.Email = email;
                    ViewBag.Link = obj1;
                    ViewBag.message = TempData["message"];
                    var query4 = (from s in db.ms_personal_data where s.pk_employee == fk_applicant select s.status_employee).FirstOrDefault();
                    var query2 = (from s in db.ms_recruit where s.fk_applicant_information_form == fk_applicant select s).ToList();
                    var query3 = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == fk_applicant && s.access_token == 1 select s).ToList();
                    int y = 0;
                    ViewBag.status = query4;

                    if (query2.Count != 0)
                    {
                        foreach (var h in query2)
                        {
                            if (h.fk_applicant_information_form != null)
                            {
                                if (query4 == 3)
                                {
                                    y += 0;
                                }
                                else
                                {
                                    var formdone = (from s in db.historical_invitation_progress where s.fk_applicant_information_form == h.fk_applicant_information_form && s.access_token == 1 select s).FirstOrDefault();
                                    formdone.applicant_information_status = 3;
                                    db.Entry(formdone).State = EntityState.Modified;
                                    db.SaveChanges();
                                    y += 1;
                                }
                                ViewBag.Form = y;
                            }
                        }
                    }

                    foreach (var z in query3)
                    {
                        if (z.disc_status == 3)
                        {
                            y += 1;
                            ViewBag.DISC = y;
                        }
                        if (z.psychotest_status == 3)
                        {
                            y += 1;
                            ViewBag.psikotes = y;
                        }
                        if (z.logictest_status == 3)
                        {
                            y += 1;
                            ViewBag.logictest = y;
                        }
                    }
                    ViewBag.progress = y;
                    if (query4 != 1)
                    {
                        ViewBag.Banyak = arrayproblem.Length - 1;
                    }
                    else
                    {
                        ViewBag.Banyak = arrayproblem.Length;
                    }
                }
                else if (linkactive.starttime >= DateTime.Now || linkactive.expiredtime <= DateTime.Now)
                {
                    return RedirectToAction("Unableaccess", "OtherPage");
                    //return RedirectToAction("NotFound", "OtherPage");
                }

            }
            else {
                return RedirectToAction("NotFound", "OtherPage");
                //return RedirectToAction("Unableaccess", "OtherPage");
            }

            return View();
        }

        public static string DecryptString(string key, string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }

    }
}