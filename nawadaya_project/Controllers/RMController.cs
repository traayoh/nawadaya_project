﻿using nawadaya_project.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace project_nawadaya.Controllers
{
    public class RMController : Controller
    {
        // GET: RM
        public ActionResult Index()
        {
            if (Session["UserName"] == null)
            {
                if (Session["UserName"] == null)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            var data3 = (from s in db.logins select s).ToList();
            var listuser = db.tbl_user.ToList();
            int countinguser = 0;
            foreach (var item in listuser) {
                countinguser++;
            }
            var listemployee = db.ms_personal_data.Where(x =>x.status_employee==3).ToList();
            int countingemployee = 0;
            foreach (var item in listemployee)
            {
                countingemployee++;
            }
            ViewBag.countuser = countinguser;
            ViewBag.countemployee = countingemployee;

            ViewBag.datalogin = data3;

            return View();
        }

        private Nawadaya_Entities db = new Nawadaya_Entities();
        //public ActionResult MsSkill()
        //{

        //    if (Session["UserName"] == null)
        //    {
        //        return RedirectToAction("Index", "Login");
        //    }
        //    else
        //    {
        //        var data = (from s in db.ms_skill select s).ToList();
        //        ViewBag.AddSkill = data;
        //    }

        //    return View();
        //}

        //[HttpPost]
        //public ActionResult AddSkill(ms_skill _post)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _post.created_by = Session["UserName"].ToString();
        //        _post.data_crated = DateTime.Now;
        //        db.ms_skill.Add(_post);
        //        db.SaveChanges();
        //        return RedirectToAction("MsSkill");

        //        //return Json(_post);
        //    }
        //    TempData["message"] = 0;
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult DeleteSkill(int id)
        //{
        //    var query = db.ms_skill.Where(s => s.pk_skill == id).First();
        //    db.ms_skill.Remove(query);
        //    db.SaveChanges();
        //    return RedirectToAction("MsSkill");
        //}

        //public ActionResult EditSkill(int id)
        //{
        //    var query = db.ms_skill.Where(s => s.pk_skill == id).First();
        //    ViewBag.EditSkill = query;
        //    var data = (from s in db.ms_skill select s).ToList();
        //    ViewBag.AddSkill = data;
        //    ViewBag.PKskill = id;
        //    return View("MsSkill", query);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult EditSkill(ms_skill item)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        var data = db.ms_skill.Find(item.pk_skill);
        //        data.skill_name = item.skill_name;
        //        data.created_by = Session["UserName"].ToString();
        //        data.data_crated = DateTime.Now;
        //        db.Entry(data).State = EntityState.Modified;
        //        db.SaveChanges();
        //        // return Json(_post);
        //        return RedirectToAction("MsSkill");
        //    }

        //    return View();
        //}

        //public ActionResult MsRole()
        //{

        //    if (Session["UserName"] == null)
        //    {
        //        return RedirectToAction("Index", "Login");
        //    }
        //    else
        //    {
        //        var data = (from s in db.ms_role select s).ToList();
        //        ViewBag.MsRole = data;
        //    }

        //    return View();
        //}

        //public ActionResult MsStatus()
        //{

        //    if (Session["UserName"] == null)
        //    {
        //        return RedirectToAction("Index", "Login");
        //    }
        //    else
        //    {
        //        var data = (from s in db.ms_status select s).ToList();
        //        ViewBag.MsStatus = data;
        //    }

        //    return View();
        //}

        //[HttpPost]
        //public ActionResult AddStatus(ms_status _post)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _post.created_by = Session["UserName"].ToString();
        //        _post.data_crated = DateTime.Now;
        //        db.ms_status.Add(_post);
        //        db.SaveChanges();
        //        return RedirectToAction("MsStatus");

        //        //return Json(_post);
        //    }
        //    TempData["message"] = 0;
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult DeleteStatus(int id)
        //{
        //    var query = db.ms_status.Where(s => s.pk_statusID == id).First();
        //    db.ms_status.Remove(query);
        //    db.SaveChanges();
        //    return RedirectToAction("MsStatus");
        //}

        //public ActionResult EditStatus(int id)
        //{
        //    var query = db.ms_status.Where(s => s.pk_statusID == id).First();
        //    ViewBag.EditSkill = query;
        //    var data = (from s in db.ms_status select s).ToList();
        //    ViewBag.MsStatus = data;
        //    ViewBag.PKskill = id;
        //    return View("MsStatus", query);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult EditStatus(ms_status item)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        var data = db.ms_status.Find(item.pk_statusID);
        //        data.employee_status_name = item.employee_status_name;
        //        data.created_by = Session["UserName"].ToString();
        //        data.data_crated = DateTime.Now;
        //        db.Entry(data).State = EntityState.Modified;
        //        db.SaveChanges();
        //        // return Json(_post);
        //        return RedirectToAction("MsStatus");
        //    }

        //    return View();
        //}

        public ActionResult AddUser()
        {

            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var max = db.tbl_user.Max(r => r.pk_userID);
                var data2 = (from s in db.employee_adduser select s).ToList();
                var data3 = (from s in db.logins where s.role_name != "Super Admin" select s).ToList();
                var data4 = (from s in db.ms_role select s).ToList();
                ViewBag.DataKaryawan = data2;
                ViewBag.datalogin = data3;
                ViewBag.datarole = data4;
                //ViewBag.MaxID = max;
                ViewBag.message = TempData["message"];
                //start/viewbag untuk modal updaterole/KMS 
                ViewBag.update_role_message = TempData["update_role_message"];
                //end/viewbag untuk modal updaterole/KMS 
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddUser(tbl_user post,string email,int role)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var key = "b14ca5898a4e4133bbce2ea2315a1916";
                        var password = md5(post.password);
                        var link = EncryptString(key, string.Format("{0}", email));
                        var new_role = (db.ms_role.Where(x => x.pk_role == role).FirstOrDefault()).role_name;
                        var validation = SendEmail(email, link, new_role);
                        if (validation == true)
                        {
                            post.password = password;
                            post.fk_role = role;
                            post.created_by = Session["UserName"].ToString();
                            post.data_crated = DateTime.Now;
                            db.tbl_user.Add(post);
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 1;
                        }
                        else
                        {
                            TempData["message"] = 0;
                        }
                        return RedirectToAction("AddUser");

                        //return Json(_post);
                    }
                    ViewBag.message = 1;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            
            return View();

        }

        public static string EncryptString(string key, string plainInput)
        {
            byte[] iv = new byte[16];
            byte[] array;
            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainInput);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }


        public bool SendEmail(string email, string link, string new_role)
        {
            MailMessage mm = new MailMessage("prewtry7@gmail.com", email);
            mm.From = new MailAddress("prewtry7@gmail.com", "Nawadaya");
            mm.Subject = "Role Assignment";
            var employ_name = (db.ms_personal_data.Where(x => x.email == email).FirstOrDefault()).full_name;
            //string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + email + ",</b><br>Nawadaya Super Admin has assigned you a role in our application. A new account has been create for you. Click the button below to create your password.<br><br> <a class='button' href='http://localhost/Nawadaya2/Login/Reset?id=" + link + "'>Reset Password</a><br>If you didn't make this request then you can safely ignore this email.";
            //string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + employ_name + ",</b><br>Nawadaya Super Admin has assigned you a role in our application.<br>A new account has been create for you. Click the button below to create your password.<br><br> <a class='button' href='http://localhost/Nawadaya2/Login/Reset?id=" + link + "'>Reset Password</a><br>If you didn't make this request then you can safely ignore this email.";
            var obj_SA = db.logins.Where(x => x.role_name == "Super Admin").FirstOrDefault();
            var SA_full_name = (obj_SA).full_name;
            var SA_email = (obj_SA).email;
            string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + employ_name + ",</b><br>Nawadaya Super Admin has assigned you a role as "+ new_role + ".<br>A new account has been created for you. Click the button below to create your password.<br><br> <a class='button' href='http://localhost/Nawadaya2/Login/Reset?id=" + link + "'>Reset Password</a><br>If you didn't make this request then you can safely ignore this email.<br><br>Best Regards,<br>" + SA_full_name + "<br>" + "Nawadaya Super Admin<br><br><b>PT Nawa Data Solutions</b><br>Prince Center Building Lt. 06 Unit 605<br>Jl Jend Sudirman KAV 3 - 4, Karet Tengsin Tanah Abang<br>Jakarta Pusat 10250" + "<br>Email: " + SA_email;
            mm.Body = tulisan;
            mm.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;

            NetworkCredential nc = new NetworkCredential("prewtry7@gmail.com", "husehusen");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = nc;
            try
            {
                smtp.Send(mm);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

        }

        public bool ChangeRoleSendEmail(string email,string new_role)
        {
            MailMessage mm = new MailMessage("prewtry7@gmail.com", email);
            mm.From = new MailAddress("prewtry7@gmail.com", "Nawadaya");
            mm.Subject = "Role Assignment Update";
            var employ_name = (db.ms_personal_data.Where(x => x.email == email).FirstOrDefault()).full_name;
            //string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + email + ",</b><br>Nawadaya Super Admin has assigned you a role in our application. A new account has been create for you. Click the button below to create your password.<br><br> <a class='button' href='http://localhost/Nawadaya2/Login/Reset?id=" + link + "'>Reset Password</a><br>If you didn't make this request then you can safely ignore this email.";
            var obj_SA = db.logins.Where(x => x.role_name == "Super Admin").FirstOrDefault();
            var SA_full_name = (obj_SA).full_name;
            var SA_email = (obj_SA).email;
            string tulisan = "<style>.button {background - color: #1c87c9;border: none;color: white;padding: 20px 34px;text - align: center;text - decoration: none;display: inline - block;font - size: 20px;margin: 4px 2px;cursor: pointer;}</style><b>Hi " + employ_name + ",</b><br>Nawadaya Super Admin has update your role.<br>Your new role is "+new_role+".<br>You can use your old password to log in to your new role.<br><br> Best Regards,<br>"+SA_full_name+ "<br>"+"Nawadaya Super Admin<br><br><b>PT Nawa Data Solutions</b><br>Prince Center Building Lt. 06 Unit 605<br>Jl Jend Sudirman KAV 3 - 4, Karet Tengsin Tanah Abang<br>Jakarta Pusat 10250" + "<br>Email: " + SA_email;
            mm.Body = tulisan;
            mm.IsBodyHtml = true;

            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;

            NetworkCredential nc = new NetworkCredential("prewtry7@gmail.com", "husehusen");
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = nc;
            try
            {
                smtp.Send(mm);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

        }

        [HttpGet]
        public ActionResult DeleteUser(int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var query = db.tbl_user.Where(s => s.pk_userID == id).First();
                    db.tbl_user.Remove(query);
                    db.SaveChanges();
                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }
            return RedirectToAction("AddUser");
        }

        public ActionResult EditUser(int id)
        {
            var query = db.tbl_user.Where(s => s.pk_userID == id).First();
            var fk = query.fk_employee;
            var email = (from s in db.ms_personal_data where s.pk_employee == fk select s).FirstOrDefault();
            ViewBag.EmailUser = email.email;
            ViewBag.Name = email.full_name;
            ViewBag.EditUser = query;
            var data4 = (from s in db.ms_role select s).ToList();
            ViewBag.datarole = data4;

            return View("AddUser", query);
        }

        [HttpPost]
        public ActionResult EditUseer(tbl_user obj, int id)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    int fk = Convert.ToInt16(obj.fk_employee);
                    var query = db.tbl_user.Where(s => s.fk_employee == fk).First();
                    query.fk_role = id;
                    //db.Entry(query).State = EntityState.Modified;
                    //db.SaveChanges();
                    //start/tambahan buat kirim email pemberitahuan pergantial role di nawadaya/Kms
                    var email_to = (db.ms_personal_data.Where(x => x.pk_employee == query.fk_employee).FirstOrDefault()).email;
                    var new_role = (db.ms_role.Where(x => x.pk_role == query.fk_role).FirstOrDefault()).role_name;
                    var validation = ChangeRoleSendEmail(email_to,new_role);
                    if (validation == true)
                    {
                        db.Entry(query).State = EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        TempData["update_role_message"] = 1;
                        TempData["message"] = null;
                    }
                    else
                    {
                        TempData["update_role_message"] = 0;
                        TempData["message"] = null;
                    }
                    return RedirectToAction("AddUser");
                    //end/tambahan buat kirim email pemberitahuan pergantial role di nawadaya/Kms
                    //transaction.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }


            return RedirectToAction("AddUser");
        }

        public ActionResult AddEmployee()
        {

            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                var data = (from s in db.ms_personal_data where s.status_employee==3 select s).ToList();
                ViewBag.MsEmployee = data;
                ViewBag.message = TempData["message"];
            }

            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(ms_personal_data _post,string position)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var Emailexist = (from s in db.ms_personal_data where s.email.Equals(_post.email) select s).ToList();
                        if (Emailexist.Count != 0)
                        {
                            TempData["message"] = 2;
                            return RedirectToAction("AddEmployee");
                        }
                        else
                        {
                            string add = "asad";
                            var d = add.Length;
                            var c = add.Substring(1);
                            var max = db.ms_personal_data.Max(r => r.NIP);
                            if (max == null) {
                                max = "NDS00000";
                            }         
                            var angka = max.Substring(3, max.Length - 3);
                            _post.NIP = format_number(Convert.ToInt16(angka) + 1);
                            _post.status_employee = 3; //3 adalah status employee untuk karyawan 
                            _post.position = position;
                            _post.created_by = Session["UserName"].ToString();
                            _post.date_created = DateTime.Now;
                            db.ms_personal_data.Add(_post);
                            db.SaveChanges();
                            var pk = (from s in db.ms_personal_data where s.email.Equals(_post.email) select s.pk_employee).FirstOrDefault();
                            int pk2 = Convert.ToInt32(pk);
                            var recruit = new ms_recruit();
                            recruit.fk_applicant_information_form = pk2;
                            recruit.created_by = Session["UserName"].ToString();
                            recruit.data_crated = DateTime.Now;
                            db.ms_recruit.Add(recruit);
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 1;

                            return RedirectToAction("AddEmployee");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }




            return RedirectToAction("AddEmployee");
        }

        public static string format_number(int number)
        {
            string str_number = number.ToString();
            int n_zeros = 8 - str_number.Length;
            string allzeroz = "NDS00000";
            string resultzeros = allzeroz.Substring(0, n_zeros);
            string string_result = resultzeros + str_number;
            return string_result;
        }

        [HttpGet]
        public ActionResult DeleteEmployee(long id)
        {
            var pk_employee = (from s in db.ms_personal_data where s.pk_employee == id select s.pk_employee).FirstOrDefault();
            var query1 = db.tbl_user.Where(s => s.fk_employee == pk_employee).FirstOrDefault();
            var query2 = db.ms_recruit.Where(s => s.fk_applicant_information_form == pk_employee).FirstOrDefault();
            var query3 = db.historical_invitation_progress.Where(s => s.fk_applicant_information_form == pk_employee).ToList();

            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (query1 != null)
                    {
                        db.tbl_user.Remove(query1);
                        db.SaveChanges();
                        if (query2 != null)
                        {
                            db.ms_recruit.Remove(query2);
                            db.SaveChanges();
                        }
                        if (query3 != null)
                        {
                            foreach (var item in query3)
                            {
                                db.historical_invitation_progress.Remove(item);
                                db.SaveChanges();
                                transaction.Commit();
                            }
                        }
                    }
                    var query = db.ms_personal_data.Where(s => s.pk_employee == id).FirstOrDefault();
                    db.ms_personal_data.Remove(query);
                    db.SaveChanges();
                    transaction.Commit();
                    TempData["message"] = 3;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }

            return RedirectToAction("AddEmployee");
        }

        public ActionResult DetailEmployee(long id)
        {
            if (Session["UserName"] == null)
            {
                return RedirectToAction("Index", "Login");
            }
            var query = (from s in db.ms_personal_data where s.pk_employee == id select s).ToList();
            var query2 = (from s in db.ms_personal_data where s.pk_employee == id select s).First();
            var query3 = (from s in db.ms_emergency_family where s.fk_applicant_information_form == id select s).ToList();
            var query4 = (from s in db.ms_family_data where s.fk_applicant_information_form == id select s).ToList();
            var query5 = (from s in db.ms_favorite_job where s.fk_applicant_information_form == id select s).ToList();
            var query6 = (from s in db.ms_formal_education where s.fk_applicant_information_form == id select s).ToList();
            var query7 = (from s in db.ms_hobby where s.fk_applicant_information_form == id select s).ToList();
            var query8 = (from s in db.ms_informal_education where s.fk_applicant_information_form == id select s).ToList();
            var query9 = (from s in db.ms_language where s.fk_applicant_information_form == id select s).ToList();
            var query10 = (from s in db.ms_organization where s.fk_applicant_information_form == id select s).ToList();
            var query11 = (from s in db.ms_question where s.fk_employee == id select s).ToList();
            var query12 = (from s in db.ms_reference where s.fk_applicant_information_form == id select s).ToList();
            var query13 = (from s in db.ms_work_experience where s.fk_applicant_information_form == id select s).ToList();
            var query14 = (from s in db.ms_skill where s.fk_applicant_information_form == id select s).ToList();
            var query15 = (from s in db.ms_recruit where s.fk_applicant_information_form == id select s).FirstOrDefault();
            ViewBag.Psychotest = query15.fk_psychotest_result;
            ViewBag.DISC = query15.fk_disc_result;
            ViewBag.Logictest = query15.fk_logictest_result;
            ViewBag.form = query15.fk_applicant_information_form;

            ViewBag.MsEmployee = query;
            ViewBag.MsEmergency = query3;
            ViewBag.MsFamily = query4;
            ViewBag.FavoriteJob = query5;
            ViewBag.FormalEducation = query6;
            ViewBag.mshobby = query7;
            ViewBag.informaledu = query8;
            ViewBag.mslanguage = query9;
            ViewBag.msorganization = query10;
            ViewBag.msquestion = query11;
            ViewBag.msreference = query12;
            ViewBag.msworkexperience = query13;
            ViewBag.msskill = query14;
            ViewBag.message = TempData["message"];

            return View("DetailEmployee", query2);
        }

        public ActionResult EditEmployee(long id)
        {
            if (Session["UserName"] != null)
            {
                RedirectToAction("Index");
            }
            var query = (from s in db.ms_personal_data where s.pk_employee == id select s).First();
            var query2 = (from s in db.ms_personal_data where s.pk_employee == id select s).ToList();
            ViewBag.MsEmployee = query2;
            ViewBag.Edit = 1;
            return View("DetailEmployee", query);
        }

        [HttpPost]
        public ActionResult EditEmployee(ms_personal_data _post)
        {
            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var Emailexist = (from s in db.ms_personal_data where s.email.Equals(_post.email) select s).ToList();
                        if (Emailexist.Count > 1)
                        {
                            TempData["message"] = 2;
                            return RedirectToAction("DetailEmployee", new { id = _post.pk_employee });
                        }
                        else
                        {
                            var data1 = (from s in db.ms_recruit where s.fk_applicant_information_form == _post.pk_employee select s.pk_recruitID).FirstOrDefault();
                            if (data1 == 0)
                            {
                                var recruit = new ms_recruit();
                                recruit.fk_applicant_information_form = _post.pk_employee;
                                recruit.created_by = Session["UserName"].ToString();
                                recruit.data_crated = DateTime.Now;
                                db.ms_recruit.Add(recruit);
                                db.SaveChanges();
                            }
                            var data = db.ms_personal_data.Find(_post.pk_employee);
                            data.address = _post.address;
                            data.birth_date = _post.birth_date;
                            data.email = _post.email;
                            data.gender = _post.gender;
                            data.home_phone = _post.home_phone;
                            data.birth_place = _post.birth_place;
                            data.marriage_status = _post.marriage_status;
                            data.full_name = _post.full_name;
                            data.NIK = _post.NIK;
                            data.phone = _post.phone;
                            data.position = _post.position;
                            data.religion = _post.religion;
                            data.status_employee = 3;
                            data.created_by = Session["UserName"].ToString();
                            data.date_created = DateTime.Now;
                            db.Entry(data).State = EntityState.Modified;
                            db.SaveChanges();
                            transaction.Commit();
                            TempData["message"] = 1;
                            return RedirectToAction("DetailEmployee", new { id = _post.pk_employee });
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    transaction.Rollback();
                }
            }


            return RedirectToAction("DetailEmployee");
        }

        static string md5(string input)
        { // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create(); // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            // Create a new Stringbuilder to collect the bytes // and create a string.
            StringBuilder sBuilder = new StringBuilder(); // Loop through each byte of the hashed data // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

    }
}