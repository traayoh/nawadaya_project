﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using nawadaya_project.Models;

namespace nawadaya_project.Controllers
{
    public class DISCResultController : Controller
    {
        private Nawadaya_Entities db = new Nawadaya_Entities();
        // GET: DISCResult
        public ActionResult IndexTES()
        {
            long tes = 1;
            ViewBag.tes = tes;
            return RedirectToAction("GraphandDescrValue", new { id = tes });
            //return View();
        }
        public ActionResult GraphandDescrValue(long id)
        {

            using (DbContextTransaction transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (id != 0)
                    {
                        //ms_disc_result obj_result = new ms_disc_result();
                        ms_disc_graph_result obj_graph_res = new ms_disc_graph_result();
                        var obj_result = db.ms_disc_result.Where(x => x.pk_disc_result == id).FirstOrDefault();
                        obj_graph_res.fk_disc_result = obj_result.pk_disc_result;

                        //Most
                        var obj_GMD = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.MD).FirstOrDefault();
                        obj_graph_res.RGMD = obj_GMD.GMD;

                        var obj_GMI = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.MI).FirstOrDefault();
                        obj_graph_res.RGMI = obj_GMI.GMI;

                        var obj_GMS = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.MS).FirstOrDefault();
                        obj_graph_res.RGMS = obj_GMS.GMS;

                        var obj_GMC = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.MC).FirstOrDefault();
                        obj_graph_res.RGMC = obj_GMC.GMC;
                        //

                        //Least
                        var obj_GLD = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.LD).FirstOrDefault();
                        obj_graph_res.RGLD = obj_GLD.GLD;

                        var obj_GLI = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.LI).FirstOrDefault();
                        obj_graph_res.RGLI = obj_GLI.GLI;

                        var obj_GLS = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.LS).FirstOrDefault();
                        obj_graph_res.RGLS = obj_GLS.GLS;

                        var obj_GLC = db.ms_graphvalue_mostleast.Where(x => x.tablevalue == (int)obj_result.LC).FirstOrDefault();
                        obj_graph_res.RGLC = obj_GLC.GLC;
                        //

                        //Change (M-L)=A
                        var obj_GAD = db.ms_graphvalue_change.Where(x => x.tablevalue == (int)obj_result.AD).FirstOrDefault();
                        obj_graph_res.RGAD = obj_GAD.GAD;

                        var obj_GAI = db.ms_graphvalue_change.Where(x => x.tablevalue == (int)obj_result.AI).FirstOrDefault();
                        obj_graph_res.RGAI = obj_GAI.GAI;

                        var obj_GAS = db.ms_graphvalue_change.Where(x => x.tablevalue == (int)obj_result.AS).FirstOrDefault();
                        obj_graph_res.RGAS = obj_GAS.GAS;

                        var obj_GAC = db.ms_graphvalue_change.Where(x => x.tablevalue == (int)obj_result.AC).FirstOrDefault();
                        obj_graph_res.RGAC = obj_GAC.GAC;
      
                        db.ms_disc_graph_result.Add(obj_graph_res);
                        db.SaveChanges();
                        

						var pk_graph_to_descr = obj_graph_res.pk_disc_graph_result;
						//
						ms_DISC_final_result obj_DISC_final = new ms_DISC_final_result();
						var obj_graph = db.ms_disc_graph_result.Where(x => x.pk_disc_graph_result == pk_graph_to_descr).FirstOrDefault();
						obj_DISC_final.fk_disc_graph_result = obj_graph.pk_disc_graph_result;

						var mostnum = countDISC((double)obj_graph.RGMD, (double)obj_graph.RGMI, (double)obj_graph.RGMS, (double)obj_graph.RGMC);
						var leastnum = countDISC((double)obj_graph.RGLD, (double)obj_graph.RGLI, (double)obj_graph.RGLS, (double)obj_graph.RGLC);
						var changenum = countDISC((double)obj_graph.RGAD, (double)obj_graph.RGAI, (double)obj_graph.RGAS, (double)obj_graph.RGAC);

						var obj_chars_most = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)mostnum).FirstOrDefault();
						var obj_chars_least = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)leastnum).FirstOrDefault();
						var obj_chars_change = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)changenum).FirstOrDefault();

						var obj_descr_change = db.ms_DISC_personal_description.Where(x => x.pk_DISC_personal_description == (long)changenum).FirstOrDefault();
						var obj_jobsmatch_change = db.ms_DISC_jobs_match.Where(x => x.pk_DISC_jobs_match == (long)changenum).FirstOrDefault();

						obj_DISC_final.mostA = obj_chars_most.a;
						obj_DISC_final.mostB = obj_chars_most.b;
						obj_DISC_final.mostC = obj_chars_most.c;
						obj_DISC_final.mostD = obj_chars_most.d;
						obj_DISC_final.mostE = obj_chars_most.e;
						obj_DISC_final.mostF = obj_chars_most.f;
						obj_DISC_final.mostG = obj_chars_most.g;
						obj_DISC_final.mostH = obj_chars_most.h;
						obj_DISC_final.mostI = obj_chars_most.i;
						obj_DISC_final.mostJ = obj_chars_most.j;
						obj_DISC_final.mostK = obj_chars_most.k;
						obj_DISC_final.mostL = obj_chars_most.l;
						obj_DISC_final.mostM = obj_chars_most.m;

						obj_DISC_final.leastA = obj_chars_least.a;
						obj_DISC_final.leastB = obj_chars_least.b;
						obj_DISC_final.leastC = obj_chars_least.c;
						obj_DISC_final.leastD = obj_chars_least.d;
						obj_DISC_final.leastE = obj_chars_least.e;
						obj_DISC_final.leastF = obj_chars_least.f;
						obj_DISC_final.leastG = obj_chars_least.g;
						obj_DISC_final.leastH = obj_chars_least.h;
						obj_DISC_final.leastI = obj_chars_least.i;
						obj_DISC_final.leastJ = obj_chars_least.j;
						obj_DISC_final.leastK = obj_chars_least.k;
						obj_DISC_final.leastL = obj_chars_least.l;
						obj_DISC_final.leastM = obj_chars_least.m;

						obj_DISC_final.changeA = obj_chars_change.a;
						obj_DISC_final.changeB = obj_chars_change.b;
						obj_DISC_final.changeC = obj_chars_change.c;
						obj_DISC_final.changeD = obj_chars_change.d;
						obj_DISC_final.changeE = obj_chars_change.e;
						obj_DISC_final.changeF = obj_chars_change.f;
						obj_DISC_final.changeG = obj_chars_change.g;
						obj_DISC_final.changeH = obj_chars_change.h;
						obj_DISC_final.changeI = obj_chars_change.i;
						obj_DISC_final.changeJ = obj_chars_change.j;
						obj_DISC_final.changeK = obj_chars_change.k;
						obj_DISC_final.changeL = obj_chars_change.l;
						obj_DISC_final.changeM = obj_chars_change.m;

						obj_DISC_final.change_descr = obj_descr_change.descr;
						obj_DISC_final.change_jobs_match = obj_jobsmatch_change.jobs_match;

						db.ms_DISC_final_result.Add(obj_DISC_final);
						db.SaveChanges();
						transaction.Commit();
						//int asdfgfsd = 1;
						//
						//return RedirectToAction("FinalValue", new { id = pk_graph_to_descr });
						return RedirectToAction("Index", "Login");

					}
                    return RedirectToAction("Index", "Login");
                }
                catch (Exception ex)
                {

                    transaction.Rollback();
                    Console.WriteLine(ex.Message);
                    return RedirectToAction("Index", "Login");

                }
            }
        }

		//(double I2, double J2, double K2, double L2)//(D, I, S, C)
		public static int countDISC(double I2, double J2, double K2, double L2)
		{
			int countDEF = 1;
			if (I2 <= 0 && J2 <= 0 && K2 <= 0 && L2 > 0)//1//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 <= 0 && K2 <= 0 && L2 <= 0)//2//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 <= 0 && K2 <= 0 && L2 > 0 && L2 >= I2)//3//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && K2 <= 0 && L2 <= 0 && J2 >= I2)//4//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && K2 <= 0 && L2 > 0 && J2 >= I2 && I2 >= L2)//5//(I2>0&&J2>0&&K2<=0&&L2>0&&J2>=I2>=L2)

			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && J2 >= I2 && I2 >= K2)//6//(I2>0&&J2>0&&K2>0&&L2<=0&&J2>=I2>=K2)
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && J2 >= K2 && K2 >= I2)//7//(I2>0&&J2>0&&K2>0&&L2<=0&&J2>=K2>=I2)
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 <= 0 && K2 > 0 && L2 > 0 && K2 >= I2 && I2 >= L2)//8//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && K2 <= 0 && L2 <= 0 && I2 >= J2)//9//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && I2 >= J2 && J2 >= K2)//10//(I2>0&&J2>0&&K2>0&&L2<=0&&I2>=J2>=K2)
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 <= 0 && K2 > 0 && L2 <= 0 && I2 >= K2)//11//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 <= 0 && J2 > 0 && K2 > 0 && L2 > 0 && L2 >= J2 && J2 >= K2)//12//(I2<=0&&J2>0&&K2>0&&L2>0&&L2>=J2>=K2)
			{
				return countDEF;
			}
			countDEF++;
			if (I2 <= 0 && J2 > 0 && K2 > 0 && L2 > 0 && L2 >= K2 && K2 >= J2)//13//(I2<=0&&J2>0&&K2>0&&L2>0&&L2>=K2>=J2)
			{
				return countDEF;
			}
			countDEF++;
			if (I2 <= 0 && J2 > 0 && K2 > 0 && L2 > 0 && J2 >= K2 && J2 >= L2)//14//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 <= 0 && J2 <= 0 && K2 > 0 && L2 <= 0)//15//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 <= 0 && J2 <= 0 && K2 > 0 && L2 > 0 && L2 >= K2)//16//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 <= 0 && J2 <= 0 && K2 > 0 && L2 > 0 && K2 >= L2)//17//
			{
				return countDEF;
			}
			countDEF++;
			if (J2 <= 0 && K2 <= 0 && I2 > 0 && L2 > 0 && I2 >= L2)//18//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && I2 >= J2 && J2 >= L2)//19//(I2>0&&J2>0&&L2>0&&K2<=0&&I2>=J2>=L2)
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && K2 > 0 && J2 > 0 && L2 <= 0 && I2 >= K2 && K2 >= J2)//20//(I2>0&&K2>0&&J2>0&&L2<=0&&I2>=K2>=J2)
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && K2 > 0 && L2 > 0 && J2 <= 0 && I2 >= K2 && K2 >= L2)//21//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && I2 >= L2 && L2 >= J2)//22//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && K2 > 0 && L2 > 0 && J2 <= 0 && I2 >= L2 && L2 >= K2)//23//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 <= 0 && K2 <= 0 && L2 <= 0 && J2 > 0)//24//
			{
				return countDEF;
			}
			countDEF++;
			if (J2 > 0 && K2 > 0 && I2 <= 0 && L2 <= 0 && J2 >= K2)//25//
			{
				return countDEF;
			}
			countDEF++;
			if (J2 > 0 && L2 > 0 && I2 <= 0 && K2 <= 0 && J2 >= L2)//26//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && J2 >= L2 && L2 >= I2)//27//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 <= 0 && J2 > 0 && K2 > 0 && L2 > 0 && J2 >= L2 && L2 >= K2)//28//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 <= 0 && K2 > 0 && L2 <= 0 && K2 >= I2)//29//
			{
				return countDEF;
			}
			countDEF++;
			if (J2 > 0 && K2 > 0 && I2 <= 0 && L2 <= 0 && K2 >= J2)//30//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && K2 >= I2 && I2 >= J2)//31//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && K2 > 0 && L2 <= 0 && K2 >= J2 && J2 >= I2)//32//
			{
				return countDEF;
			}
			countDEF++;
			if (J2 > 0 && K2 > 0 && L2 > 0 && I2 <= 0 && K2 >= J2 && J2 >= L2)//33//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 <= 0 && K2 > 0 && L2 > 0 && K2 >= L2 && L2 >= I2)//34//
			{
				return countDEF;
			}
			countDEF++;
			if (J2 > 0 && K2 > 0 && L2 > 0 && I2 <= 0 && K2 >= L2 && L2 >= J2)//35//
			{
				return countDEF;
			}
			countDEF++;
			if (J2 > 0 && L2 > 0 && I2 <= 0 && K2 <= 0 && L2 >= J2)//36//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && L2 >= I2 && I2 >= J2)//37//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && K2 > 0 && L2 > 0 && J2 <= 0 && L2 >= I2 && I2 >= K2)//38//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && J2 > 0 && L2 > 0 && K2 <= 0 && L2 >= J2 && J2 >= I2)//39//
			{
				return countDEF;
			}
			countDEF++;
			if (I2 > 0 && K2 > 0 && L2 > 0 && J2 <= 0 && L2 >= K2 && K2 >= I2)//40//
			{
				return countDEF;
			}
			countDEF++;

			return countDEF;
		}

		//public ActionResult FinalValue(long id)
		//{

		//	using (DbContextTransaction transaction = db.Database.BeginTransaction())
		//	{
		//		try
		//		{
		//			if (id != 0)
		//			{
						
		//				ms_DISC_final_result obj_DISC_final = new ms_DISC_final_result();
		//				var obj_graph = db.ms_disc_graph_result.Where(x => x.pk_disc_graph_result == id).FirstOrDefault();
		//				obj_DISC_final.fk_disc_graph_result = obj_graph.pk_disc_graph_result;

		//				var mostnum = countDISC((double)obj_graph.RGMD, (double)obj_graph.RGMI, (double)obj_graph.RGMS, (double)obj_graph.RGMC);
		//				var leastnum = countDISC((double)obj_graph.RGLD, (double)obj_graph.RGLI, (double)obj_graph.RGLS, (double)obj_graph.RGLC);
		//				var changenum = countDISC((double)obj_graph.RGAD, (double)obj_graph.RGAI, (double)obj_graph.RGAS, (double)obj_graph.RGAC);

		//				var obj_chars_most = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)mostnum).FirstOrDefault();
		//				var obj_chars_least = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)leastnum).FirstOrDefault();
		//				var obj_chars_change = db.ms_DISC_personal_characters.Where(x => x.pk_DISC_personal_characters == (long)changenum).FirstOrDefault();

		//				var obj_descr_change = db.ms_DISC_personal_description.Where(x => x.pk_DISC_personal_description == (long)changenum).FirstOrDefault();
		//				var obj_jobsmatch_change = db.ms_DISC_jobs_match.Where(x => x.pk_DISC_jobs_match == (long)changenum).FirstOrDefault();

		//				obj_DISC_final.mostA = obj_chars_most.a;
		//				obj_DISC_final.mostB = obj_chars_most.b;
		//				obj_DISC_final.mostC = obj_chars_most.c;
		//				obj_DISC_final.mostD = obj_chars_most.d;
		//				obj_DISC_final.mostE = obj_chars_most.e;
		//				obj_DISC_final.mostF = obj_chars_most.f;
		//				obj_DISC_final.mostG = obj_chars_most.g;
		//				obj_DISC_final.mostH = obj_chars_most.h;
		//				obj_DISC_final.mostI = obj_chars_most.i;
		//				obj_DISC_final.mostJ = obj_chars_most.j;
		//				obj_DISC_final.mostK = obj_chars_most.k;
		//				obj_DISC_final.mostL = obj_chars_most.l;
		//				obj_DISC_final.mostM = obj_chars_most.m;

		//				obj_DISC_final.leastA = obj_chars_least.a;
		//				obj_DISC_final.leastB = obj_chars_least.b;
		//				obj_DISC_final.leastC = obj_chars_least.c;
		//				obj_DISC_final.leastD = obj_chars_least.d;
		//				obj_DISC_final.leastE = obj_chars_least.e;
		//				obj_DISC_final.leastF = obj_chars_least.f;
		//				obj_DISC_final.leastG = obj_chars_least.g;
		//				obj_DISC_final.leastH = obj_chars_least.h;
		//				obj_DISC_final.leastI = obj_chars_least.i;
		//				obj_DISC_final.leastJ = obj_chars_least.j;
		//				obj_DISC_final.leastK = obj_chars_least.k;
		//				obj_DISC_final.leastL = obj_chars_least.l;
		//				obj_DISC_final.leastM = obj_chars_least.m;

		//				obj_DISC_final.changeA = obj_chars_change.a;
		//				obj_DISC_final.changeB = obj_chars_change.b;
		//				obj_DISC_final.changeC = obj_chars_change.c;
		//				obj_DISC_final.changeD = obj_chars_change.d;
		//				obj_DISC_final.changeE = obj_chars_change.e;
		//				obj_DISC_final.changeF = obj_chars_change.f;
		//				obj_DISC_final.changeG = obj_chars_change.g;
		//				obj_DISC_final.changeH = obj_chars_change.h;
		//				obj_DISC_final.changeI = obj_chars_change.i;
		//				obj_DISC_final.changeJ = obj_chars_change.j;
		//				obj_DISC_final.changeK = obj_chars_change.k;
		//				obj_DISC_final.changeL = obj_chars_change.l;
		//				obj_DISC_final.changeM = obj_chars_change.m;

		//				obj_DISC_final.change_descr = obj_descr_change.descr;
		//				obj_DISC_final.change_jobs_match = obj_jobsmatch_change.jobs_match;

		//				db.ms_DISC_final_result.Add(obj_DISC_final);
		//				db.SaveChanges();
		//				transaction.Commit();
		//				int asdf = 10;

		//				//var pk_graph_to_descr = obj_graph_res.pk_disc_graph_result;
		//				return RedirectToAction("Index", "Login");
		//				//return RedirectToAction("Index", "Login");

		//			}
		//			return RedirectToAction("Index", "Login");
		//		}
		//		catch (Exception ex)
		//		{

		//			transaction.Rollback();
		//			Console.WriteLine(ex.Message);
		//			return RedirectToAction("Index", "Login");

		//		}
		//	}
		//}
	}
}