//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace nawadaya_project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ms_all_for_one
    {
        public long pk_all_for_one { get; set; }
        public string var_name { get; set; }
        public string var_unit { get; set; }
        public string var_value { get; set; }
        public string updated_by { get; set; }
        public Nullable<System.DateTime> date_updated { get; set; }
    }
}
