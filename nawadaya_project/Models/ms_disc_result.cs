//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace nawadaya_project.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ms_disc_result
    {
        public long pk_disc_result { get; set; }
        public Nullable<int> fk_applicant_information_form { get; set; }
        public Nullable<int> MD { get; set; }
        public Nullable<int> MI { get; set; }
        public Nullable<int> MS { get; set; }
        public Nullable<int> MC { get; set; }
        public Nullable<int> LD { get; set; }
        public Nullable<int> LI { get; set; }
        public Nullable<int> LS { get; set; }
        public Nullable<int> LC { get; set; }
        public Nullable<int> AD { get; set; }
        public Nullable<int> AI { get; set; }
        public Nullable<int> AS { get; set; }
        public Nullable<int> AC { get; set; }
        public Nullable<int> xM { get; set; }
        public Nullable<int> xL { get; set; }
        public Nullable<int> Total_M { get; set; }
        public Nullable<int> Total_L { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public Nullable<System.DateTime> endtime { get; set; }
    }
}
