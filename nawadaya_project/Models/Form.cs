﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace nawadaya_project.Models
{
    public class Form
    {
        public ms_personal_data data_pribadi { get; set; }
        public List<ms_files> data_files { get; set; }
        public List<ms_language>  bahasa_lain { get; set; }
        public List<ms_family_data> data_keluarga { get; set; }
        public List<ms_hobby> hobby { get; set; }
        public List<ms_emergency_family> keluarga_darurat { get; set; }
        public List<ms_skill> keterampilan { get; set; }
        public List<ms_organization> organisasi { get; set; }
        public List<ms_favorite_job>  pekerjaan_favorite { get; set; }
        public List<ms_formal_education> pendidikan_formal { get; set; }
        public List<ms_informal_education> pendidikan_informal { get; set; }
        public List<ms_work_experience> pengalaman_kerja { get; set; }
        public List<ms_reference> referensi { get; set; }
        public ms_question pertanyaan { get; set; }
        

    }
    
}