﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace nawadaya_project.Models
{
    public class SelectType
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
    public class Family
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Country
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
    public class Religion
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
    public class Education
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}